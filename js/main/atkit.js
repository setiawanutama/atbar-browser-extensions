/*!
 * AtKit Core
 * Open Source Cross-Browser ToolBar framework
 *
 * Copyright (c) 2011. University of Southampton
 * Developed by Sebastian Skuse
 * and further by Magnus White and Nawar Halabi
 * http://www.atbar.org/
 *
 * Licensed under the BSD Licence.
 * http://www.opensource.org/licenses/bsd-license.php
 *
 */
(function (window) {
    var AtKit = (function () {
        // Default CSS attributes settings that help enhance the interoperability of the toolbar by preventing pages' css form affecting the toolbar's. Add it before you declare any css to any element in the toolbar or dialogs
        var default_css = "-webkit-box-sizing: content-box; -moz-box-sizing: content-box; box-sizing: content-box; height:initial; width:initial; margin:0px; padding:0; border:0; vertical-align:baseline; font-size: initial; font: inherit; float:none; line-height:initial; color:initial; background:initial;";

        // API object. Everything in here becomes public after AtKit has finished executing
        var API = {};

        // Internal properties
        // AtKit.internal = AtKit.prototype = {
        AtKit.internal = {
            __version: 1.6, // Version.
            __build: 2, // Build.
            __APIVersion: 1.0, // The version of the API.
            __baseURL: "https://core.atbar.org/", // Load AtKit assets from here.
            __APIURL: "http://a.atbar.org/", // API endpoint
            __pluginURL: "https://plugins.atbar.org/", // Plugins location
            __faceboxURL: "https://core.atbar.org/resources/js/facebox.dev.js", // Facebox JS lib
            __libURL: "https://core.atbar.org/resources/jquery/1.8/jquery.min.js", // URL to jQuery. CDN preferred unless this is a local install.
            // __bootstrapJsURL: "https://core.atbar.org/resources/js/bootstrap.min.js",
            // __bootstrapCssURL: "https://core.atbar.org/resources/css/bootstrap.min.css",
            __responsiveCssURL: "https://core.atbar.org/resources/css/responsive.css",
            __channel: "atkit", // Release channel we're running in for this version of AtKit.
            __invoked: false, // Is the framework already loaded?
            __debug: false, // Debug mode on or off.
            __loadAttempts: 0, // Container for number of load attempts
            __maxLoadAttempts: 30, // Maximum number of times we'll try and load the library (one try every 500ms)
            __errorMessageTimeout: 2000, // Time before error message will disapear.
            __localStorageNamespace: "AtKit_", // Name to use for HTML5 localstorage namespace
            __protocol: null, // HTTPS or HTTP
            plugins: {}, // Plugins
            localisations: {
                en: {
                    exit: "Exit",
                    reset: "Reset webpage",
                    help: "Help & instructions",
                    collapse: "Collapse and uncollapse plugins"
                },
                ar: {
                    exit: "&#1582;&#1585;&#1608;&#1580;",
                    reset: "&#1575;&#1604;&#1605;&#1581;&#1575;&#1608;&#1604;&#1577; &#1605;&#1580;&#1583;&#1583;&#1575;",
                    help: "&#1605;&#1587;&#1575;&#1593;&#1583;&#1577; &#1608; &#1605;&#1593;&#1604;&#1608;&#1605;&#1575;&#1578; &#1575;&#1590;&#1575;&#1601;&#1610;&#1577;",
                    collapse: "&#1593;&#1585;&#1590; &#1571;&#1608; &#1573;&#1582;&#1601;&#1575;&#1569; &#1575;&#1604;&#1604;&#1575;&#1574;&#1581;&#1577;"
                }
            },
            templates: {
                global: {
                    buttons: {},
                    dialogs: {}, // Global dialogs (can be called through API.show)
                    storage: {}, // Global settings (API.set() API.get())
                    fn: {}, // Global functions (can be called through API.call)
                    unloadFn: {}, // Functions to run when we unload
                    helpFn: {},
                    resetFn: {},
                    closeFn: {}
                }
            },
            debugCallback: null,
            language: "en",
            defaultLanguage: "en"
        };

        // if runs as browser extension, get local URL of plugins
        if (window.isExtension) {
            if (window.extensionBrowser === "chrome") {
                AtKit.internal.__pluginURL = chrome.runtime.getURL("js/main/plugins/");
                AtKit.internal.__responsiveCssURL = chrome.runtime.getURL("resources/css/responsive.css");
            } else if (window.extensionBrowser === "firefox") {
                AtKit.internal.__pluginURL = browser.runtime.getURL("js/main/plugins/");
                AtKit.internal.__responsiveCssURL = browser.runtime.getURL("resources/css/responsive.css");
            } else if (window.extensionBrowser === "edge") {
                AtKit.internal.__pluginURL = browser.runtime.getURL("js/main/plugins/");
                AtKit.internal.__responsiveCssURL = browser.runtime.getURL("resources/css/responsive.css");
            }
        }

        AtKit.internal.__resourceURL = AtKit.internal.__baseURL;
        AtKit.internal.__channelURL += AtKit.internal.__channel;
        AtKit.internal.__assetURL = AtKit.internal.__baseURL + "resources/";
        AtKit.internal.versionString = "v" + AtKit.internal.__version.toFixed(1) + "." + AtKit.internal.__build + " (" + AtKit.internal.__channel + " release channel)";

        if (window.isExtension) {
            if (window.extensionBrowser === "chrome") {
                AtKit.internal.__assetURL = chrome.runtime.getURL("resources/");
            } else if (window.extensionBrowser === "firefox") {
                AtKit.internal.__assetURL = browser.runtime.getURL("resources/");
            } else if (window.extensionBrowser === "edge") {
                AtKit.internal.__assetURL = browser.runtime.getURL("resources/");
            }
        }

        AtKit.internal.__aboutDialog = {
            CSS: {
                "#ATKFBAbout": "font-family:Helvetica, Verdana, Arial, sans-serif; font-size:12px; color:#364365; direction: ltr; text-align:left",
                "#ATKFBAbout h2": "border-bottom:1px solid #DDD; font-size:16px; margin-bottom:5px; margin-top:10px; padding-bottom:5px; direction: ltr; text-align:left",
                "#ATKFBAbout p#ATKFBAboutFooter": "border-top:1px solid #DDD; padding-top:10px; margin-top:10px;"
            }
        };

        // Public object that affects how AtKit behaves. Host toolbar has access to this.
        // AtKit.external = AtKit.prototype = {
        AtKit.external = {
            transport: "JSONP", // AJAX transport method.
            window: window, // Reference for the window object we're using.
            global: AtKit.internal.templates.global,
            buttons: {}, // Object for every button. Object with the layout: { identifier: { function: function(), tip: 'tip', state: 'enabled' } }
            languageMap: {}, // Translations
            siteFixes: [] // Contains object for each site {regex: /regex/, function: function()} //
        };

        API.__env = AtKit.external; // Load public object into API accessible object
        API.__templates = {
            barGhost: "<center><img src=\"" + AtKit.internal.__assetURL + "img/loading.gif\" style=\"margin-top:10px;\" /></center>",
            barFailed: "<center>library loading failed</center>",
            button: "<div id=\"at-btn-(ID)\" title=\"(TITLE)\" class=\"at-btn (CLASS)\"><a title=\"(TITLE)\" id=\"at-lnk-(ID)\" href=\"#ATBarLink\"><img src=\"(SRC)\" alt=\"(TITLE)\" height=\"16\" width=\"16\" border=\"0\" /></a></div>",
            spacer: "<div class=\"at-spacer\"></div>",
            separator: "<div class=\"at-separator at-separator-(ID)\"></div>"
        };
        API.__CSS = {
            "#sbarGhost, #sbar": default_css,
            "#sbarGhost *, #sbar *": default_css,
            "#sbarlogo": default_css + "float:left",
            "#sbarlogo img": default_css + "margin-top:10px; vertical-align:middle;",
            "#sbar": default_css + "height:40px; left:0; line-height:40px; margin-left:auto; margin-right:auto; position:fixed; top:0;width:100%; z-index:2147483646; padding:0 5px; background:url(" + AtKit.internal.__assetURL + "img/background.png) repeat-x #EBEAED;",
            "#sbarGhost": default_css + "height:40px; width:100%;",
            "#at-collapse": "",
            ".at-spacer": default_css + "display:block; height:40px; width:40px; float:left",
            ".at-separator": default_css + "display:block; height:25px; border-left:2px solid #a9a9a9; margin:7px 1px 4px 7px; float:left;",
            ".at-btn": default_css + "height:28px; width:28px; line-height:14px; text-align:center; color:#FFF; clear:none; margin:5px 0 0 5px;background:url(" + AtKit.internal.__assetURL + "img/button_background.png) no-repeat; float:left;",
            ".at-btn a": default_css + "display:block; height:28px; width:28px; background:transparent; position:inherit;",
            ".at-btn a:active": default_css + "font-size: 100%; font: inherit; border:yellow solid 2px;",
            ".at-btn img": default_css + "padding:6px; default_css + border:none; background:none;",
            ".no-float": "float: none !important;",
            "#at-btn-atkit-reset, #at-btn-atkit-unload, #at-btn-atkit-help, #at-btn-atkit-toggle": default_css + "height:28px; width:28px; line-height:14px; text-align:center; color:#FFF; clear:none; float:right; margin:5px 10px 0 0; background:url(" + AtKit.internal.__assetURL + "img/button_background.png) no-repeat;",
            "#at-facebox > .at-popup > table .at-fb-tb-body > .at-fb-footer": default_css + "border-top-width: 1px; border-top-style: solid; border-top-color: rgb(221, 221, 221); padding-top:5px; margin-top:10px; display:block; text-align:left;",
            "#at-facebox > .at-popup > table .at-fb-tb-body > .at-fb-footer > a ": default_css + "float: left;",
            "#at-facebox > .at-popup > table .at-fb-tb-body > .at-fb-footer > a > img ": default_css + "float: left;",
            "#at-facebox > .at-popup > table .at-fb-tb-body > .at-fb-content > .user-dialog > h1": default_css + "font-family:Helvetica Neue,Helvetica,Arial,sans-serif; font-size:18pt; font-weight:bold; color:black; display:block; margin:10px 0px 10px 0px;"/* float:left;" */,
            "#at-facebox > .at-popup > table .at-fb-tb-body > .at-fb-content h1": default_css + "font-family:Helvetica Neue,Helvetica,Arial,sans-serif; font-size:18pt; font-weight:bold; color:black; display:block; margin:10px 0px 10px 0px;"/* float:left;" */,
            "#at-facebox > .at-popup > table .at-fb-tb-body > .at-fb-content h2": default_css + "font-family:Helvetica Neue,Helvetica,Arial,sans-serif; font-size:16pt; font-weight:bold; color:black; display:block; margin:5px 0px 5px 0px;"/* float:left;" */,
            "#at-facebox > .at-popup > table .at-fb-tb-body > .at-fb-content p": default_css + "display:block;" /* float:left;" */,
            "#at-facebox > .at-popup > table .at-fb-tb-body > .at-fb-content > .user-dialog > p": default_css + "display:block;" /* float:left;" */,
            "#at-facebox > .at-popup > table .at-fb-tb-body > .at-fb-content button": default_css + "font-family:Helvetica Neue,Helvetica,Arial,sans-serif; height:26px; margin:10px; padding:5px; color:white; background-color:#0064CD; border-color:rgba(0,0,0,0.1) rgba(0,0,0,0.1) rgba(0,0,0,0.25); text-shadow:0 -1px 0 rgba(0,0,0,0.25); background-image: -webkit-linear-gradient(top, #049cdb, #0064cd); border-radius:4px;",
            "#at-facebox > .at-popup > table .at-fb-tb-body > .at-fb-content label": default_css + "float:left; display:block; margin:0px; margin-right:5px;",
            "#at-facebox > .at-popup > table .at-fb-tb-body > .at-fb-content select": default_css + "border:1px solid black; float:left; display:block;",
            "#at-facebox > .at-popup > table .at-fb-tb-body > .at-fb-content input": default_css + "border:1px solid black; float:left; display:block;",
            "#at-facebox > .at-popup > table .at-fb-tb-body > .at-fb-content select#spellchecksuggestions": default_css + "border:1px solid black; float:none; display:block;width:360px;",
            "#at-facebox > .at-popup > table .at-fb-tb-body > .at-fb-content select#spellcheckmistakes": default_css + "border:1px solid black; float:none; display:block;width:360px;"
        };
        API.settings = {
            noiframe: true, // Don't load if we're in an iframe.
            allowclose: true, // Enable the close button
            allowreset: true, // Allow the page reset button
            allowhelp: true, // Allow the page help button
            isRightToLeft: false, // Switch for changing to right to left orientation
            logoURL: "",
            name: "",
            about: ""
        };
        API.htmlTags = ["a", "abbr", "acronym", "address", "applet", "area", "article", "aside", "audio", "b", "base", "basefont", "bdi", "bdo", "big", "blockquote", "body", "br", "button", "canvas", "caption", "center", "cite", "code", "col", "colgroup", "command", "datalist", "dd", "del", "details", "dfn", "dir", "div", "dl", "dt", "em", "embed", "fieldset", "figcaption", "figure", "font", "footer", "form", "frame", "frameset", "h1", "h2", "h3", "h4", "h5", "h6", "head", "header", "hgroup", "hr", "html", "i", "iframe", "img", "input", "ins", "kbd", "keygen", "label", "legend", "li", "link", "map", "mark", "menu", "meta", "meter", "nav", "noframes", "noscript", "object", "ol", "optgroup", "option", "output", "p", "param", "pre", "progress", "q", "rp", "rt", "ruby", "s", "samp", "script", "section", "select", "small", "source", "span", "strike", "strong", "style", "sub", "summary", "sup", "table", "tbody", "td", "textarea", "tfoot", "th", "thead", "time", "title", "tr", "track", "tt", "u", "ul", "var", "video", "wbr"];
        API.version = AtKit.internal.__APIVersion;
        API.$ = ""; // Library used for the Toolbar

        function Plugin(name) {
            var $ = API.$;

            // Data & settings
            this.name = name;
            this.supportedLanguages = [];
            this.aboutDialog = "";
            this.settings = {};
            this.version = 0;

            // Events
            this.onRender = function () {
                return undefined;
            };
            this.onRun = function () {
                return undefined;
            };

            // Register plugin
            this.register = function () {
                AtKit.registerPlugin(this.name, this);
            };

            // Fired by AtKit when we are ready to render plugin.
            // Don't call this yourself.
            this.run = function () {
                this.onRun($);
            };

            // Fired by AtKit when we actually render.
            // Don't call this yourself.
            this.render = function () {
                this.onRender($);
            };
        }

        API.plugin = function (name) {
            return new Plugin(name);
        };

        // ////////////////////////////
        // Private internal methods //
        // ////////////////////////////

        function debug(msg) {
            if (!AtKit.internal.__debug) return;
            if (AtKit.internal.debugCallback != null) {
                AtKit.internal.debugCallback(msg);
            } else if (typeof console !== "undefined") {
                console.log(msg);
            }
        }

        function broadcastLoaded() {
            debug("broadcastLoaded fired.");

            // return API to the global namespace.
            window.AtKit = API;

            // Send event to the plugin, if a listener has been defined.
            if (window.AtKitLoaded !== undefined) {
                window.AtKitLoaded.fire(null, { version: AtKit.internal.__version });
            }
        }

        // Attach a javascript file to the DOM
        function attachJS(identifier, url) {
            var j = document.createElement("script");
            j.src = url;
            j.type = "text/javascript";
            j.id = identifier;
            document.getElementsByTagName("head")[0].appendChild(j);
        }

        // Attach a CSS file to the DOM
        function attachCss(identifier, url) {
            var j = document.createElement("link");
            j.href = url;
            j.rel = "stylesheet";
            j.type = "text/css";
            j.id = identifier;
            document.getElementsByTagName("head")[0].appendChild(j);
        }

        // Called when loading of the library failed and we've given up waiting.
        function loadFailed() {
            var bar = document.getElementById("sbarGhost");

            bar.innerHTML = API.__templates.barFailed;

            setTimeout(function () {
                var body = document.getElementsByTagName("body");
                bar = document.getElementById("sbarGhost");
                body[0].removeChild(bar);
            }, AtKit.internal.__errorMessageTimeout);
        }

        function waitForLib() {
            debug("waitForLib invoked");
            // If we are at the max attempt count, stop.
            if (AtKit.internal.__loadAttempts === AtKit.internal.__maxLoadAttempts) {
                debug("Max load count reached: stopping execution.");
                loadFailed();
                return;
            }

            // Check to see if jQuery has loaded. If not set a timer and increment the loadAttempts (so we don't flood the user if site is inacessible)
            if (typeof window.jQuery === "undefined" || window.jQuery == null) {
                debug("waitForLib: jQuery undefined.");
                setTimeout(function () {
                    waitForLib();
                }, 100);
                AtKit.internal.__loadAttempts += 1;
            } else {
                // Bind jQuery to internal namespace.
                API.$ = window.jQuery.noConflict(true);

                if (typeof window._jQuery !== "undefined") {
                    window.jQuery = window._jQuery;
                }
                if (typeof window._$ !== "undefined") {
                    window.$ = window._$;
                }

                // Once the document is ready broadcast ready event.
                API.$(document).ready(function () {
                    broadcastLoaded();
                });
            }
        }

        // load jQuery library
        function loadLibrary() {
            debug("loadLibrary called");
            // Do we have a jQuery library loaded already?

            // load the custom built responsive css. We did not use bootstrap because there might a version conflict
            if (window.isExtension) {
                // tell background script to load CSS
                if (window.extensionBrowser === "chrome") {
                    chrome.runtime.sendMessage({ message: "load_local_css", filename: "/resources/css/responsive.css" });
                } else if (window.extensionBrowser === "firefox") {
                    browser.runtime.sendMessage({ message: "load_local_css", filename: "/resources/css/responsive.css" });
                } else if (window.extensionBrowser === "edge") {
                    browser.runtime.sendMessage({ message: "load_local_css", filename: "/resources/css/responsive.css" });
                }
            } else {
                attachCss("atkit-responsive-css", AtKit.internal.__responsiveCssURL);
            }

            // load Modernizr
            if (window.Modernizr === undefined) {
                // load Modernizr 2.8.3 from CDN, it'd better to use latest version
                AtKit.addScript("https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js", null);
            }

            if (typeof window.jQuery !== "undefined") {
                try {
                    // We need jQuery 1.5 or above. Get the version string.
                    const jQversion = parseFloat(window.jQuery().jquery.match(/\d\.\d/));
                    debug("jQuery already loaded, v" + jQversion);

                    if (jQversion > 1.5) {
                        debug("loaded version acceptable, using.");
                        API.$ = window.jQuery;

                        broadcastLoaded();

                        return;
                    }
                    window._jQuery = window.jQuery;
                    window.jQuery = null;
                } catch (error) {
                    console.error(error);
                }
            } else if (typeof window.$ !== "undefined") {
                window._$ = window.$;
            }

            if (AtKit.internal.__debug) {
                const newVersion = parseFloat(AtKit.internal.__libURL.match(/\d\.\d/));
                debug("jQuery not loaded, loading " + newVersion);
            }
            // jQuery not loaded. Attach.
            if (window.isExtension) {
                // tell background script to load script
                if (window.extensionBrowser === "chrome") {
                    chrome.runtime.sendMessage({ message: "load_local_library", filename: "/js/lib/jquery.min.js" });
                } else if (window.extensionBrowser === "firefox") {
                    browser.runtime.sendMessage({ message: "load_local_library", filename: "/js/lib/jquery.min.js" });
                } else if (window.extensionBrowser === "edge") {
                    browser.runtime.sendMessage({ message: "load_local_library", filename: "/js/lib/jquery.min.js" });
                }
            } else {
                attachJS("atkit-jquery", AtKit.internal.__libURL);
            }

            // load bootstrap
            // attachJS( 'atkit-bootstrap-js', AtKit.internal.__bootstrapJsURL );
            // attachJS( 'atkit-bootstrap-css', AtKit.internal.__bootstrapCssURL );

            // Wait for library to load.
            waitForLib();
        }

        // show the loading div, defined in templates variable in the API.
        function showLoader() {
            // Create the div for the AtKit ghost.
            var barGhost = document.createElement("div");
            // Set the ID of the toolbar.
            barGhost.id = "sbarGhost";
            barGhost.innerHTML = API.__templates.barGhost;

            if (document.body != null) {
                document.body.insertBefore(barGhost, document.body.firstChild);
            } else {
                const bodyCheck = setInterval(function () {
                    if (document.body != null) {
                        // Insert it as the first node in the body node.
                        document.body.insertBefore(barGhost, document.body.firstChild);
                        clearInterval(bodyCheck);
                    }
                }, 100);
            }
        }

        // Functions below here (but above the API functions) run with NO jQuery loaded.

        // checks to see if the sbar element is loaded into the DOM.
        function isLoaded() {
            if (window.isExtension) {
                if (!AtKit.internal.__invoked) {
                    // Destroy the toolbar created by Atbar Lite. If the DOM has been built by ATbar Lite, .getElementById("sbar") will return true, which blocks Atbar from getting loaded in extension mode
                    let el = document.getElementById("sbarGhost");
                    if (el !== null && el !== undefined) el.remove();
                    el = document.getElementById("sbar");
                    if (el !== null && el !== undefined) el.remove();
                    el = document.getElementsByClassName("tooltip");
                    if (el !== null && el !== undefined && el.length > 0) {
                        for (let i = 0; i < el.length; i += 1) {
                            el[i].remove();
                        }
                    }
                }
            }

            if (document.getElementById("sbar") != null) return true;

            return false;
        }

        // Bootstrap function
        function bootstrap() {
            debug("bootstrapping AtKit " + AtKit.internal.versionString + "...");
            // If we're invoked already don't load again.
            if (AtKit.internal.__invoked || isLoaded()) {
                return;
            }

            // Don't load if we're not the top window (running in an iframe)
            if (API.settings.noiframe && window !== window.top) {
                return;
            }

            if (typeof window.AtKitLoaded !== "undefined") {
                showLoader();
            }

            // Set window, if we're running in GreaseMonkey, we'll need access to unsafeWindow rather than window.
            if (typeof unsafeWindow === "undefined") {
                AtKit.external.window = window;
            } else {
                AtKit.external.window = unsafeWindow;
                AtKit.external.transport = "GM-XHR";
            }

            // Load Library
            loadLibrary();
        }

        function loadFacebox() {
            if (API.$.facebox === undefined) {
                if (window.isExtension) {
                    // tell background script to load facebox script
                    if (window.extensionBrowser === "chrome") {
                        chrome.runtime.sendMessage({ message: "load_local_library", filename: "/js/main/facebox.dev.js" });
                    } else if (window.extensionBrowser === "firefox") {
                        browser.runtime.sendMessage({ message: "load_local_library", filename: "/js/main/facebox.dev.js" });
                    } else if (window.extensionBrowser === "edge") {
                        browser.runtime.sendMessage({ message: "load_local_library", filename: "/js/main/facebox.dev.js" });
                    }
                } else {
                    API.addScript(AtKit.internal.__faceboxURL);
                }
            }
        }

        // AtKit may break some websites. Authors of toolbars are able, through attachSiteFix, fix any issues with sites.
        function siteFixes() {
            debug("siteFixes fired. Running fixes.");
            if (API.__env.siteFixes.length === 0) {
                return;
            }

            API.__env.siteFixes.forEach(function (value) {
                const sf = value;
                if (sf.regex.test()) {
                    sf.f();
                }
            });
        }

        function renderButton(ident) {
            // Pull down the template.
            var b = API.__templates.button;

            debug("renderButton fired for ident " + ident + ".");

            // Replace in the template.
            b = b.replace(/\(ID\)/ig, ident);
            b = b.replace(/\(TITLE\)/ig, API.__env.buttons[ident].tooltip);
            b = b.replace(/\(SRC\)/ig, API.__env.buttons[ident].icon);
            b = b.replace(/\(CLASS\)/ig, API.__env.buttons[ident].cssClass);

            // jQuery'ify
            b = API.$(b);

            // Bind the click event and pass in reference to the button object
            b.children("a").bind("click touchend", { button: API.__env.buttons[ident] }, function (button) {
                try {
                    API.__env.buttons[ident].action(button.data.button.dialogs, button.data.button.functions);

                    // Callback to log the click to GA
                    API.send_analytics_event({
                        hitType: "event",
                        eventCategory: "Plugin." + ident,
                        eventAction: "invoked"
                    });
                } catch (err) {
                    debug(err);

                    // Callback to log the error to GA
                    API.send_analytics_event({
                        hitType: "event",
                        eventCategory: "Plugin." + ident,
                        eventAction: "error"
                    });
                }

                button.preventDefault();
            });

            // Emulate CSS active, hover, etc.
            b.children("a").bind("focus", function () {
                API.$(this).attr("style", API.$(this).attr("style") + API.__CSS[".at-btn a:active"]);
            });

            b.children("a").bind("focusout", function () {
                API.$(this).attr("style", API.__CSS[".at-btn a"]);
            });

            // Commit the HTML
            API.__env.buttons[ident].HTML = b;

            // Return the HTML
            return API.__env.buttons[ident].HTML;
        }

        // Apply the CSS rules that have been defined
        function applyCSS(obj) {
            var cssObj = (typeof obj === "undefined") ? API.__CSS : obj;

            for (const c in cssObj) {
                if (/:active/.test(c) || API.$(c).length === 0) {
                    continue;
                }

                try {
                    // Get CSS item
                    let property = cssObj[c];

                    // Are we running in RTL mode?
                    if (API.settings.isRightToLeft) {
                        const floatRight = "float:right";
                        const floatLeft = "float:left";

                        // Does the string contain floatleft?
                        let match;
                        if (property.indexOf(floatLeft) !== -1) {
                            match = new RegExp(floatLeft, "gi");
                            property = property.replace(match, floatRight);
                        } else if (property.indexOf(floatRight) !== -1) {
                            // Does it contain floatright? if so switch.
                            match = new RegExp(floatRight, "gi");
                            property = property.replace(match, floatLeft);
                        }
                    }

                    // Apply the CSS
                    API.$(c).attr("style", property);
                } catch (e) {
                    debug(e.description);
                }
            }
        }

        function showAbout() {
            var plugins = API.listPlugins();

            // Create the dialog
            AtKit.internal.__aboutDialog.HTML = "<h2>About " + API.settings.name + "</h2>";

            // Append user text
            AtKit.internal.__aboutDialog.HTML += "<p id='ATKFBUserSpecifiedAbout'>" + API.settings.about + "</p>";

            // Append AtKit text
            AtKit.internal.__aboutDialog.HTML += "<p id='ATKFBAboutFooter'>Powered by AtKit " + AtKit.internal.versionString;

            if (plugins.length > 0) {
                AtKit.internal.__aboutDialog.HTML += "<br /> Registered plugins: ";

                plugins.map(function (el) {
                    AtKit.internal.__aboutDialog.HTML += "<button class='pluginLink'>" + el + "</button>";
                    return el;
                });
            }

            AtKit.internal.__aboutDialog.HTML += "</p>";

            // Convert to jQuery object & wrap
            AtKit.internal.__aboutDialog.HTML = API.$("<div>", { id: "ATKFBAbout" }).append(AtKit.internal.__aboutDialog.HTML);

            API.message(AtKit.internal.__aboutDialog.HTML);
            applyCSS(AtKit.internal.__aboutDialog.CSS);
        }

        // Private function used to actually start the toolbar.
        function start() {
            // Are we in RTL mode? Work out where we should be positioned.
            var align = API.settings.isRightToLeft ? "right" : "left";

            // Load facebox.
            loadFacebox();

            // Bootstrap Analytics
            API.bootstrap_analytics("UA-24512836-5");

            // If we're already invoked ignore.
            if (AtKit.internal.__invoked || isLoaded()) {
                return;
            }

            if (API.$("#sbarGhost").length === 0) {
                showLoader();
            }

            // Create <div id="sbar"></div> element on the fly and then insert it after the bar holder
            API.$(API.$("<div>", { id: "sbar" })).insertAfter("#sbarGhost");

            // Insert the logo.
            API.$(
                API.$("<a>", { id: "sbarlogo", click: function () { showAbout(); } }).append(
                    API.$("<img>", {
                        src: API.settings.logoURL, align: align, border: "0", title: API.settings.name + "Logo", alt: API.settings.name + "Logo", style: "margin-top:10px;float:" + align
                    })
                )
            ).appendTo("#sbar");

            API.$("<img>", { src: "https://misc.services.atbar.org/stats/stat.php?channel=" + AtKit.internal.__channel + "-" + API.settings.name + "&version=" + AtKit.internal.__version.toFixed(1) + "." + AtKit.internal.__build, alt: " " }).appendTo("#sbar");


            // add the close button (if we have been told to use this)
            if (API.settings.allowclose) {
                API.addButton("atkit-unload", API.localisation("exit"), AtKit.internal.__assetURL + "img/close.png", function () { API.close(); }, null, null, { cssClass: "fright" });
            }

            // add the reset button (if we have been told to use this)
            if (API.settings.allowreset) {
                API.addButton("atkit-reset", API.localisation("reset"), AtKit.internal.__assetURL + "img/reset.png", function () { API.reset(); }, null, null, { cssClass: "fright" });
            }

            // add the help button (if we have been told to use this)
            if (API.settings.allowhelp) {
                API.addButton("atkit-help", API.localisation("help"), AtKit.internal.__assetURL + "img/help.png", function () { API.help(); }, null, null, { cssClass: "fright" });
            }

            // Add a collapsible container before adding the plugin buttons.
            API.$(API.$("<div>", { id: "at-collapse" })).appendTo("#sbar");

            // Add a button that collapses the toolbar plugin buttons when in small devices.
            API.addButton("atkit-toggle", API.localisation("collapse"), AtKit.internal.__assetURL + "img/collapse.gif", function () {
                if (API.$("#at-collapse").is(":visible")) {
                    API.$("#at-collapse").slideUp("fast", function () {
                        API.$("#at-collapse").addClass("at-hidden");
                        API.$("#at-collapse").removeClass("at-visible");
                    });
                } else {
                    API.$("#at-collapse").slideDown("fast", function () {
                        API.$("#at-collapse").addClass("at-visible");
                        API.$("#at-collapse").removeClass("at-hidden");
                    });
                }
            }, null, null, { cssClass: "fright" });

            // Add buttons. Only add the plugin buttons to the collapsible div
            for (const b in API.__env.buttons) {
                if (Object.prototype.hasOwnProperty.call(API.__env.buttons, b)) {
                    if (API.__env.buttons[b].cssClass === "fright") {
                        API.$(renderButton(b)).appendTo("#sbar");
                    } else {
                        API.$(renderButton(b)).appendTo("#at-collapse");
                    }
                }
            }

            // Apply CSS
            applyCSS();

            // Apply site fixes
            siteFixes();

            // IE 6 fix
            if (API.$.browser === "msie" && API.$.browser.version === 6) {
                API.$("#sbarGhost").remove();
            } else {
                API.$("#sbarGhost").html("&nbsp;");
            }

            // Set state to invoked.
            AtKit.internal.__invoked = true;

            // Set unload function
            API.__env.global.unloadFn.default = function () {
                API.$("#sbarGhost, #sbar").fadeOut(400, function () {
                    API.$(this).remove();
                });
                API.$("div.tooltip").fadeOut(400, function () {
                    API.$(this).remove();
                });
            };

            API.__env.global.resetFn.default = function () {
                window.location.reload(true);
            };

            // API.$("body").trigger("AtKitRenderComplete");
        }

        // Shut down the toolbar
        function stop() {
            // Run unload functions
            for (const f in API.__env.global.unloadFn) {
                if (Object.prototype.hasOwnProperty.call(API.__env.global.unloadFn, f)) {
                    API.__env.global.unloadFn[f]();
                }
            }

            // Cleanup.

            // Reset language
            AtKit.internal.language = AtKit.internal.defaultLanguage;

            // Reset debug callback
            AtKit.internal.debugCallback = null;

            // Reset any stored global settings.
            API.__env.global = AtKit.internal.templates.global;

            // Reset buttons.
            API.__env.buttons = {};

            // Reset any language maps defined.
            API.__env.languageMap = {};

            // Reset site fixes.
            API.__env.siteFixes = [];

            // Reset plugins
            AtKit.internal.plugins = {};

            // Set not invoked.
            AtKit.internal.__invoked = false;
        }

        // ///////////////////////
        // API functions below //
        // ///////////////////////

        API.getVersion = function () {
            return AtKit.internal.__version.toFixed(1) + "." + AtKit.internal.__build;
        };

        API.isRendered = function () {
            return AtKit.internal.__invoked;
        };

        API.g = function () {
            return AtKit.internal.__resourceURL;
        };

        API.getResourceURL = function () {
            return AtKit.internal.__resourceURL;
        };

        API.getChannelURL = function () {
            return AtKit.internal.__channelURL;
        };

        API.getPluginURL = function () {
            return AtKit.internal.__pluginURL;
        };

        // Set toolbar name
        API.setName = function (name) {
            API.settings.name = name;
        };

        API.setAbout = function (aboutText) {
            API.settings.about = aboutText;
        };

        // Set toolbar logo
        API.setLogo = function (logo) {
            API.settings.logoURL = logo;
        };

        // Set whether the toolbar is running in RTL mode.
        API.setIsRightToLeft = function (isRTL) {
            API.settings.isRightToLeft = isRTL;
        };

        // Add a CSS rule. Identifier is a jQuery selector expression, eg #bar. inlineStyle appears in the style attr in the DOM.
        API.setCSS = function (identifier, inlineStyle) {
            API.__CSS[identifier] = inlineStyle;
        };

        API.getHtmlTags = function () {
            return API.htmlTags;
        };

        // Set the language that this toolbar uses
        API.setLanguage = function (language) {
            AtKit.internal.language = language;
        };

        API.getLanguage = function () {
            return AtKit.internal.language;
        };

        // Add a localisation string (value) referenced by key for the language specified in cc.
        API.addLocalisation = function (cc, key, value) {
            AtKit.internal.localisations[cc][key] = value;
        };

        API.addLocalisationMap = function (cc, map) {
            AtKit.internal.localisations[cc] = API.$.extend(true, AtKit.internal.localisations[cc], map);
        };

        // Get a localisation string.
        API.localisation = function (key) {
            if (typeof AtKit.internal.localisations[AtKit.internal.language] === "undefined") return AtKit.internal.localisations[AtKit.internal.defaultLanguage][key];
            if (typeof AtKit.internal.localisations[AtKit.internal.language][key] === "undefined") {
                // return "{no value set for key " + key + " in language " + AtKit.internal.language + "}";
                return AtKit.internal.localisations[AtKit.internal.defaultLanguage][key];
            }
            return AtKit.internal.localisations[AtKit.internal.language][key];
        };

        // Add a site fix.
        API.addFix = function (regex, f) {
            API.__env.siteFixes.push({ regex: regex, f: f });
        };

        // Attach a JS file to the current document using jQuery, or if not loaded, the native function we have.
        API.addScript = function (url, callback) {
            if (typeof API.$ !== "undefined") {
                if (API.$("script[src=\"" + url + "\"]").length > 0) return;
                API.$.getScript(url, callback);
            } else {
                attachJS("", url);
            }
        };

        API.addStylesheet = function (url, id) {
            API.$("head").append(
                API.$("<link>", {
                    rel: "stylesheet",
                    href: url,
                    type: "text/css",
                    id: id
                })
            );
        };

        // Add a global function
        API.addFn = function (identifier, fn) {
            API.__env.global.fn[identifier] = fn;
        };

        // Add a function to be run on exit.
        API.addCloseFn = function (identifier, fn) {
            API.__env.global.closeFn[identifier] = fn;
        };

        /**
         * Add function to be run on stopping ATbar
         * @param {string} identifier
         * @param {function} fn
         */
        API.addUnloadFn = function (identifier, fn) {
            API.__env.global.unloadFn[identifier] = fn;
        };

        API.addResetFn = function (identifier, fn) {
            API.__env.global.resetFn[identifier] = fn;
        };

        API.addHelpFn = function (identifier, fn) {
            API.__env.global.helpFn[identifier] = fn;
        };

        // Add a global dialog
        API.addDialog = function (identifier, title, body) {
            API.__env.global.dialogs[identifier] = { title: title, body: body };
        };

        // Attach a button to the toolbar
        // Assets should be an object containing any dialogs that will be shown with facebox, as well a
        API.addButton = function (identifier, tooltip, icon, action, dialogs, functions, options) {
            if (typeof API.__env.buttons[identifier] !== "undefined") return;
            API.__env.buttons[identifier] = {
                icon: icon,
                tooltip: tooltip,
                action: action,
                dialogs: dialogs,
                functions: functions
            };

            if (options != null) {
                API.__env.buttons[identifier] = API.$.extend(true, API.__env.buttons[identifier], options);
            }

            if (AtKit.internal.__invoked) {
                // If the toolbar buttons have already been rendered
                API.$(renderButton(identifier)).appendTo("#at-collapse");
                applyCSS();
            }
        };

        // Remove a button from the toolbar
        API.removeButton = function (identifier) {
            delete API.__env.buttons[identifier];

            if (AtKit.internal.__invoked) {
                debug("remove button " + identifier);
                // If we've already been rendered we need to remove it from the DOM, too.
                API.$("#at-btn-" + identifier).remove();
            }
        };

        // Adds a spacer - a gap the size of a button
        API.addSpacer = function (width) {
            if (typeof width === "undefined") {
                API.$(API.__templates.spacer).appendTo("#sbar");
            }

            // if (!isNaN(width)) {
            if (typeof width === "number") {
                for (let i = 0; i < width; i += 1) {
                    API.$(API.__templates.spacer).appendTo("#sbar");
                }
            }
            applyCSS();
        };

        // Adds a separator - a line between buttons
        API.addSeparator = function (ident) {
            var s = API.__templates.separator;

            if (typeof ident === "undefined") {
                ident = "separator" + Math.floor((Math.random() * 999) + 1);
            }

            // Pull down the template
            s = s.replace(/\(ID\)/ig, ident);

            // jQuery'ify
            s = API.$(s);
            // s.appendTo('#sbar'); Separators are now added to the collapsible area
            s.appendTo("#at-collapse");
            applyCSS();
        };

        // Remove a separator with class from the toolbar
        API.removeSeparator = function (ident) {
            if (AtKit.internal.__invoked) {
                debug("remove separator " + ident);
                API.$(".at-separator-" + ident).each(function () {
                    this.remove();
                });
            }
        };

        // Load code for plugins
        API.importPlugins = function (plugins, callback) {
            if (window.isExtension) {
                const arr_plugin = [];

                if (plugins instanceof Array && plugins.length > 0) {
                    for (let i = 0; i < plugins.length; i += 1) {
                        arr_plugin.push("/js/main/plugins/" + plugins[i] + ".js");
                    }
                }
                if (arr_plugin.length > 0) {
                    // tell background script to load all plugins
                    if (window.extensionBrowser === "chrome") {
                        chrome.runtime.sendMessage({ message: "load_multiple_local_libraries", filenames: arr_plugin }, function () {
                            // wait 100 ms to let all plugins finish loading
                            const intervalId = setInterval(function () {
                                const number_of_plugin = plugins.length;
                                for (let i = 0; i < number_of_plugin; i += 1) {
                                    const identifier = plugins[i];
                                    if (Object.prototype.hasOwnProperty.call(AtKit.internal.plugins, identifier) && Object.prototype.hasOwnProperty.call(AtKit.internal.plugins[identifier], "payload") && AtKit.internal.plugins[identifier].payload !== undefined && (i === number_of_plugin - 1)) {
                                        callback();
                                        clearInterval(intervalId);
                                    }
                                }
                            }, 100);
                        });
                    } else if (window.extensionBrowser === "firefox") {
                        browser.runtime.sendMessage({ message: "load_multiple_local_libraries", filenames: arr_plugin }).then(function () {
                            // wait 100 ms to let all plugins finish loading
                            const intervalId = setInterval(function () {
                                const number_of_plugin = plugins.length;
                                for (let i = 0; i < number_of_plugin; i += 1) {
                                    const identifier = plugins[i];
                                    if (Object.prototype.hasOwnProperty.call(AtKit.internal.plugins, identifier) && Object.prototype.hasOwnProperty.call(AtKit.internal.plugins[identifier], "payload") && AtKit.internal.plugins[identifier].payload !== undefined && (i === number_of_plugin - 1)) {
                                        callback();
                                        clearInterval(intervalId);
                                    }
                                }
                            }, 100);
                        });
                    } else if (window.extensionBrowser === "edge") {
                        browser.runtime.sendMessage({ message: "load_multiple_local_libraries", filenames: arr_plugin }, function () {
                            // wait 100 ms to let all plugins finish loading
                            const intervalId = setInterval(function () {
                                const number_of_plugin = plugins.length;
                                for (let i = 0; i < number_of_plugin; i += 1) {
                                    const identifier = plugins[i];
                                    if (Object.prototype.hasOwnProperty.call(AtKit.internal.plugins, identifier) && Object.prototype.hasOwnProperty.call(AtKit.internal.plugins[identifier], "payload") && AtKit.internal.plugins[identifier].payload !== undefined && (i === number_of_plugin - 1)) {
                                        callback();
                                        clearInterval(intervalId);
                                    }
                                }
                            }, 100);
                        });
                    }
                }
            } else {
                const pluginString = (plugins instanceof Array) ? plugins.join(",") : plugins;
                API.addScript(AtKit.internal.__pluginURL + pluginString + ".js", callback);
            }
        };

        // Add plugin to rendering queue.
        API.addPlugin = function (identifier) {
            AtKit.internal.plugins[identifier].payload();
        };

        // Register a plugin (called by plugin)
        API.registerPlugin = function (identifier, plugin, metadata) {
            AtKit.internal.plugins[identifier] = { payload: plugin, metadata: metadata };
        };

        // Return an array of plugin names.
        API.listPlugins = function () {
            var pluginList = [];
            for (const p in AtKit.internal.plugins) {
                if (Object.prototype.hasOwnProperty.call(AtKit.internal.plugins, p)) {
                    pluginList.push(p);
                }
            }

            return pluginList;
        };

        API.getPluginMetadata = function (plugin) {
            return AtKit.internal.plugins[plugin].metadata;
        };

        // Pass in a dialog and we'll format it and show to the users.
        API.show = function (dialog, callback) {
            dialog = API.$("<div>", { class: "userDialog" }).append(
                API.$("<h2>", { html: dialog.title }),
                API.$("<p>", { html: dialog.body })
            );

            API.$("body").find(".facebox_hide").remove();

            API.$.facebox(dialog);

            applyCSS();

            if (callback !== null && typeof callback !== "undefined") {
                callback();
            }
        };

        // Show message not stored in a dialog object.
        API.message = function (data, callback) {
            API.$("body").find(".facebox_hide").remove();

            API.$.facebox(data);

            applyCSS();

            if (callback !== null && typeof callback !== "undefined") {
                callback();
            }
        };

        API.hideDialog = function () {
            API.$(window.document).trigger("close.facebox");
        };

        // Call a global function
        API.call = function (identifier, args) {
            return API.__env.global.fn[identifier](args);
        };

        // Set session storage variable k to v.
        API.set = function (k, v) {
            API.__env.global.storage[k] = v;
        };

        // Get session storage variable set to k.
        API.get = function (k) {
            return API.__env.global.storage[k];
        };

        // Is HTML5 localstorage available?
        API.storageAvailable = function () {
            const mod = "test";
            try {
                window.localStorage.setItem(mod, mod);
                window.localStorage.removeItem(mod);
                return true;
            } catch (e) {
                return false;
            }
        };

        // HTML5 LocalStorage
        // AtKit.storage(k[, v]);
        API.storage = function (key, value) {
            var namespaceKey = AtKit.internal.__localStorageNamespace + API.settings.name + "_" + key;

            if (!API.storageAvailable()) {
                return false;
            }

            if (typeof value === "undefined") {
                return window.localStorage.getItem(namespaceKey);
            }

            window.localStorage.setItem(namespaceKey, value);
            return true;
        };

        API.clearStorage = function () {
            var namespaceKey = AtKit.internal.__localStorageNamespace + API.settings.name;
            var exp = new RegExp("^" + namespaceKey + ".*");

            if (!API.storageAvailable()) {
                return;
            }

            for (const s in window.localStorage) {
                if (s.match(exp)) {
                    window.localStorage.removeItem(s);
                }
            }
        };

        API.setDebugger = function (fn) {
            AtKit.internal.debugCallback = fn;
        };

        // Return library.
        API.lib = function () {
            if (typeof API.$ === "function") return API.$;
            if (typeof API.$ === "string" && typeof window.jQuery === "function") return window.jQuery;
            return false;
        };

        // Toolbar calls this to render the bar.
        API.render = function () {
            start();
        };

        // Called to close the toolbar
        API.close = function (destroy_toolbar_only) {
            // hide facebox dialog
            API.hideDialog();

            let disable_atbar_fn = null;
            if (destroy_toolbar_only) {
                // don't disable atbar, just destroy the toolbar
                if (Object.prototype.hasOwnProperty.call(API.__env.global.unloadFn, "disable_atbar")) {
                    disable_atbar_fn = API.__env.global.unloadFn.disable_atbar;
                    delete API.__env.global.unloadFn.disable_atbar;
                }
            }

            stop();

            // reassign the unload function
            if (disable_atbar_fn !== null) {
                API.__env.global.unloadFn.disable_atbar = disable_atbar_fn;
            }
        };

        API.reset = function () {
            for (const f in API.__env.global.resetFn) {
                if (Object.prototype.hasOwnProperty.call(API.__env.global.resetFn, f)) {
                    API.__env.global.resetFn[f]();
                }
            }
            AtKit.internal.__invoked = false;
        };

        API.help = function () {
            const languages = ["en", "ar"];
            if (languages.indexOf(AtKit.internal.language) > -1) {
                window.open("http://" + AtKit.internal.language + ".wiki.atbar.org/", "_blank");
            } else {
                window.open("http://" + AtKit.internal.defaultLanguage + ".wiki.atbar.org/", "_blank");
            }
        };

        API.analytics_tracker_name = "atkit";

        // Loads Google Analytics
        // @property_id                 GA Property ID
        // @options         optional    options array for GA (cookies, etc)
        // @tracker_name    optional    name for this tracker, to prevent overwriting default
        API.bootstrap_analytics = function (property_id) {
            debug("bootstrapping GA");
            // If analytics.js is not already in the process of loading, invoke it ourselves
            if (window.GoogleAnalyticsObject === undefined) {
                debug("making new GA");

                // Set the ATBar analytics function name
                window.GoogleAnalyticsObject = "ga";

                // Prep an event queue for GA
                (window[window.GoogleAnalyticsObject] = function () {
                    (window[window.GoogleAnalyticsObject].q = window[window.GoogleAnalyticsObject].q || []).push(arguments);
                });

                // Store invoke time
                window[window.GoogleAnalyticsObject].ga = +new Date();

                if (window.isExtension) {
                    // tell background script to load library
                    if (window.extensionBrowser === "chrome") {
                        chrome.runtime.sendMessage({ message: "load_local_library", filename: "/js/lib/analytics.js" });
                    } else if (window.extensionBrowser === "firefox") {
                        browser.runtime.sendMessage({ message: "load_local_library", filename: "/js/lib/analytics.js" });
                    } else if (window.extensionBrowser === "edge") {
                        browser.runtime.sendMessage({ message: "load_local_library", filename: "/js/lib/analytics.js" });
                    }
                } else {
                    // Load analytics.js
                    API.$.ajax({
                        dataType: "script",
                        cache: true,
                        url: "https://www.google-analytics.com/analytics.js"
                    });
                }
            } else {
                debug("using existing GA");
            }

            // Add a callback to the GA event queue to prep a tracker.
            window[window.GoogleAnalyticsObject](function () {
                // Get a new tracker, just for ATKit
                // See https://developers.google.com/analytics/devguides/collection/analyticsjs/tracker-object-reference

                window[window.GoogleAnalyticsObject]("create", property_id, "auto", API.analytics_tracker_name);

                debug("initialised GA for " + property_id);
            });

            // Log the load event to GA
            API.send_analytics_event({
                hitType: "event",
                eventCategory: "ATKit",
                eventAction: "loaded"
            });
        };

        // Sends an event to GA
        // @event   Event to send. For details, see:
        //          https://developers.google.com/analytics/devguides/collection/analyticsjs/events)
        API.send_analytics_event = function (event) {
            // Append the referer as the current page
            event.referrer = window.location;
            debug("Queueing event:");
            debug(event);
            window[window.GoogleAnalyticsObject](function () {
                debug("Sending event with " + API.analytics_tracker_name + ".send:");
                debug(event);
                window[window.GoogleAnalyticsObject](API.analytics_tracker_name + ".send", event);
            });
        };

        // Bootstrap the application
        bootstrap();

        return API;
    });

    window.AtKit = new AtKit();
}(window));
