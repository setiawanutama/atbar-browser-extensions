(function (AtKit) {
    var pluginName = "insipio-tts";
    var plugin = function () {
        var $lib = AtKit.lib();

        var settings = {
            baseURL: "https://core.atbar.org/",
            speechServicesURL: "https://speech.services.atbar.org/",
            ttsChunkSize: 400
        };

        // var a;// HTML5 audio element
        var audio;// HTMLAudioElement
        var playPromise;// Promise returned when calling .play()

        var split_long_text = true;// sending the text all at once? or we can split into chunks

        // Text-To-Speech
        var TTSDialogs = {};
        var TTSFunctions = {};

        // CLD language map
        var cld_languages = {
            en: "English", eng: "English", da: "Danish", dan: "Danish", nl: "Dutch", dut: "Dutch", fi: "Finnish", fin: "Finnish", fr: "French", fre: "French", de: "German", ger: "German", he: "Hebrew", heb: "Hebrew", it: "Italian", ita: "Italian", ja: "Japanese", jpn: "Japanese", ko: "Korean", kor: "Korean", nb: "Norwegian", no: "Norwegian", nor: "Norwegian", pl: "Polish", pol: "Polish", pt: "Portuguese", por: "Portuguese", ru: "Russian", rus: "Russian", es: "Spanish", spa: "Spanish", sv: "Swedish", swe: "Swedish", zh: "Chinese", chi: "Chinese", "zh-CN": "Chinese", cs: "Czech", cze: "Czech", el: "Greek", gre: "Greek", is: "Icelandic", ice: "Icelandic", lv: "Latvian", lav: "Latvian", lt: "Lithuanian", lit: "Lithuanian", ro: "Romanian", rum: "Romanian", hu: "Hungarian", hun: "Hungarian", et: "Estonian", est: "Estonian", bg: "Bulgarian", bul: "Bulgarian", hr: "Croatian", scr: "Croatian", sr: "Serbian", scc: "Serbian", ga: "Irish", gle: "Irish", gl: "Galician", glg: "Galician", fil: "Tagalog", tr: "Turkish", tur: "Turkish", uk: "Ukrainian", ukr: "Ukrainian", hi: "Hindi", hin: "Hindi", mk: "Macedonian", mac: "Macedonian", bn: "Bengali", ben: "Bengali", id: "Indonesian", ind: "Indonesian", la: "Latin", lat: "Latin", ms: "Malay", may: "Malay", ml: "Malayalam", mal: "Malayalam", cy: "Welsh", wel: "Welsh", ne: "Nepali", nep: "Nepali", te: "Telugu", tel: "Telugu", sq: "Albanian", alb: "Albanian", ta: "Tamil", tam: "Tamil", be: "Belarusian", bel: "Belarusian", jv: "Javanese", jw: "Javanese", jav: "Javanese", oc: "Occitan", oci: "Occitan", ur: "Urdu", urd: "Urdu", bh: "Bihari", bih: "Bihari", gu: "Gujarati", guj: "Gujarati", th: "Thai", tha: "Thai", ar: "Arabic", ara: "Arabic", ca: "Catalan", cat: "Catalan", eo: "Esperanto", epo: "Esperanto", eu: "Basque", baq: "Basque", ia: "Interlingua", ina: "Interlingua", kn: "Kannada", kan: "Kannada", pa: "Punjabi", pan: "Punjabi", gd: "Scots Gaelic", gla: "Scots Gaelic", sw: "Swahili", swa: "Swahili", sl: "Slovenian", slv: "Slovenian", mr: "Marathi", mar: "Marathi", mt: "Maltese", mlt: "Maltese", vi: "Vietnamese", vie: "Vietnamese", fy: "Frisian", fry: "Frisian", sk: "Slovak", slo: "Slovak", "zh-TW": "ChineseT", fo: "Faroese", fao: "Faroese", su: "Sundanese", sun: "Sundanese", uz: "Uzbek", uzb: "Uzbek", am: "Amharic", amh: "Amharic", az: "Azerbaijani", aze: "Azerbaijani", ka: "Georgian", geo: "Georgian", ti: "Tigrinya", tir: "Tigrinya", fa: "Persian", per: "Persian", bs: "Bosnian", bos: "Bosnian", si: "Sinhalese", sin: "Sinhalese", nn: "Norwegian N", nno: "Norwegian N", "pt-PT": "Portuguese P", "pt-BR": "Portuguese B", xh: "Xhosa", xho: "Xhosa", zu: "Zulu", zul: "Zulu", gn: "Guarani", grn: "Guarani", st: "Sesotho", sot: "Sesotho", tk: "Turkmen", tuk: "Turkmen", ky: "Kyrgyz", kir: "Kyrgyz", br: "Breton", bre: "Breton", tw: "Twi", twi: "Twi", yi: "Yiddish", yid: "Yiddish", sh: "Serbo Croatian", so: "Somali", som: "Somali", ug: "Uighur", uig: "Uighur", ku: "Kurdish", kur: "Kurdish", mn: "Mongolian", mon: "Mongolian", hy: "Armenian", arm: "Armenian", lo: "Laothian", lao: "Laothian", sd: "Sindhi", snd: "Sindhi", rm: "Rhaeto Romance", roh: "Rhaeto Romance", af: "Afrikaans", afr: "Afrikaans", lb: "Luxembourgish", ltz: "Luxembourgish", my: "Burmese", bur: "Burmese", km: "Khmer", khm: "Khmer", bo: "Tibetan", tib: "Tibetan", dv: "Dhivehi", div: "Dhivehi", syr: "Syriac", "sit-NP": "Limbu", or: "Oriya", ori: "Oriya", as: "Assamese", asm: "Assamese", co: "Corsican", cos: "Corsican", ie: "Interlingue", ine: "Interlingue", kk: "Kazakh", kaz: "Kazakh", ln: "Lingala", lin: "Lingala", mo: "Moldavian", mol: "Moldavian", ps: "Pashto", pus: "Pashto", qu: "Quechua", que: "Quechua", sn: "Shona", sna: "Shona", tg: "Tajik", tgk: "Tajik", tt: "Tatar", tat: "Tatar", to: "Tonga", tog: "Tonga", yo: "Yoruba", yor: "Yoruba", cpe: "Creoles and Pidgins English Based", cpf: "Creoles and Pidgins French Based", cpp: "Creoles and Pidgins Portuguese Based", crp: "Creoles and Pidgins Other", mi: "Maori", mao: "Maori", wo: "Wolof", wol: "Wolof", ab: "Abkhazian", abk: "Abkhazian", aa: "Afar", aar: "Afar", ay: "Aymara", aym: "Aymara", ba: "Bashkir", bak: "Bashkir", bi: "Bislama", bis: "Bislama", dz: "Dzongkha", dzo: "Dzongkha", fj: "Fijian", fij: "Fijian", kl: "Greenlandic", kal: "Greenlandic", ha: "Hausa", hau: "Hausa", ht: "Haitian Creole", ik: "Inupiak", ipk: "Inupiak", iu: "Inuktitut", iku: "Inuktitut", ks: "Kashmiri", kas: "Kashmiri", rw: "Kinyarwanda", kin: "Kinyarwanda", mg: "Malagasy", mlg: "Malagasy", na: "Nauru", nau: "Nauru", om: "Oromo", orm: "Oromo", rn: "Rundi", run: "Rundi", sm: "Samoan", smo: "Samoan", sg: "Sango", sag: "Sango", sa: "Sanskrit", san: "Sanskrit", ss: "Siswant", ssw: "Siswant", ts: "Tsonga", tso: "Tsonga", tn: "Tswana", tsn: "Tswana", vo: "Volapuk", vol: "Volapuk", za: "Zhuang", zha: "Zhuang", kha: "Khasi", sco: "Scots", lg: "Ganda", lug: "Ganda", gv: "Manx", glv: "Manx", "sr-ME": "Montenegrin"
        };

        // This variable contains the selected text. it is global because on IPhones nad IPads selection is removed after clicking the plug-in button, so we need to preserve the selected text
        var selectedData = "";
        var selectedDataIOS = "";

        /* var TTSExtendedObject = {
            clickEnabled: true,
            positition: "",
            playingItem: "",
            "TTSButtons": {
                'ttsPlay': {
                    'tooltip': AtKit.localisation("tts_playpause"),
                    'icon': AtKit.getPluginURL() + "images/control-pause.png",
                    'fn': function(){
                        var targetObj = ($lib.browser == "msie") ? swfobject.getObjectById(AtKit.get('ATAudioPlayerID')) : window.document['audioe'];
                        targetObj.sendEvent('play');
                    }
                },
                'ttsRewind': {
                    'tooltip': AtKit.localisation("tts_rewind"),
                    'icon': AtKit.getPluginURL() + "images/control-stop-180.png",
                    'fn': function(){
                        var scrubAmount = 2;
                        var currentPosition = AtKit.get("TTS_position");
                        var newPosition = (currentPosition - scrubAmount);
                        if(newPosition < 0) newPosition = 0;

                        var targetObj = ($lib.browser == "msie") ? swfobject.getObjectById(AtKit.get('ATAudioPlayerID')) : window.document['audioe'];
                        targetObj.sendEvent('seek', newPosition);
                    }
                },
                'ttsStop': {
                    'tooltip': AtKit.localisation("tts_stop"),
                    'icon': AtKit.getPluginURL() + "images/control-stop-square.png",
                    'fn': function(){
                        var targetObj = ($lib.browser == "msie") ? swfobject.getObjectById(AtKit.get('ATAudioPlayerID')) : window.document['audioe'];
                        targetObj.sendEvent('stop');

                        AtKit.call('TTSRemoveControlBox');
                    }
                }
            }
        }; */

        // Internationalisation
        AtKit.addLocalisationMap("en", {
            tts_title: "Text to Speech",
            tts_ssapi_select_text: "Select some text and press the TTS button to read it.",
            tts_options: "Text to Speech Options",
            tts_converting: "Text to Speech conversion is taking place.",
            tts_timeremaining: "Time Remaining:",
            tts_pleasewait: "Please wait...",
            tts_playpause: "Play / Pause",
            tts_rewind: "Rewind",
            tts_stop: "Stop & Close TTS",
            tts_error: "Error",
            tts_overloaded: "The server is currently over capacity for text to speech conversions. Please try again later.",
            tts_problem: "Something went wrong while we were converting this page to speech. Please try again shortly.",
            tts_servererror: "An error occurred on the server. Please try again later.",
            tts_seconds: "seconds",
            tts_explain: "To use the text to speech feature with selected text, please first select the text on this page that you would like to convert. After you have done this, click the Text to Speech button, and select the 'selected text' option.",
            tts_select_voice: "Select a voice",
            tts_male: "Male",
            tts_female: "Female",
            tts_detected_language: "Detected language: ",
            tts_fetch_error: "There has been a problem with the fetch operation: ",
            tts_param_error: "TTS is unavailable for current parameters."
        });

        AtKit.addLocalisationMap("ar", {
            tts_title: "&#1578;&#1581;&#1608;&#1610;&#1604; &#1575;&#1604;&#1606;&#1589;&#1608;&#1589; &#1575;&#1604;&#1610; &#1605;&#1575;&#1583;&#1577; &#1605;&#1587;&#1605;&#1608;&#1593;&#1577;",
            tts_ssapi_select_text: "حدد بعض النص واضغط على زر TTS لقراءته.",
            tts_options: "&#1582;&#1610;&#1575;&#1585;&#1575;&#1578; &#1606;&#1591;&#1602; &#1575;&#1604;&#1606;&#1589;",
            tts_converting: "&#1580;&#1575;&#1585;&#1610;&#1577; &#1581;&#1575;&#1604;&#1610;&#1575;&#1611; &#1593;&#1605;&#1604;&#1610;&#1577; &#1606;&#1591;&#1602; &#1575;&#1604;&#1606;&#1589;",
            tts_timeremaining: "&#1575;&#1604;&#1608;&#1602;&#1578; &#1575;&#1604;&#1605;&#1578;&#1576;&#1602;&#1610;",
            tts_pleasewait: "&#1575;&#1604;&#1585;&#1580;&#1575;&#1569; &#1575;&#1604;&#1575;&#1606;&#1578;&#1592;&#1575;&#1585;...",
            tts_playpause: "&#1578;&#1588;&#1594;&#1610;&#1604;/&#1573;&#1610;&#1602;&#1575;&#1601; &#1605;&#1572;&#1602;&#1578;",
            tts_rewind: "&#1573;&#1593;&#1575;&#1583;&#1577;",
            tts_stop: "&#1573;&#1610;&#1602;&#1575;&#1601;",
            tts_error: "&#1582;&#1591;&#1571;",
            tts_overloaded: "&#1601;&#1575;&#1602;&#1578; &#1593;&#1605;&#1604;&#1610;&#1575;&#1578; &#1606;&#1591;&#1602; &#1575;&#1604;&#1606;&#1589;&#1608;&#1589; &#1587;&#1593;&#1577; &#1575;&#1604;&#1582;&#1575;&#1583;&#1605;. &#1575;&#1604;&#1585;&#1580;&#1575;&#1569; &#1575;&#1604;&#1605;&#1581;&#1575;&#1608;&#1604;&#1577; &#1604;&#1575;&#1581;&#1602;&#1575;&#1611;.",
            tts_problem: "&#1581;&#1583;&#1579; &#1582;&#1591;&#1571; &#1571;&#1579;&#1606;&#1575;&#1569; &#1593;&#1605;&#1604;&#1610;&#1577; &#1606;&#1591;&#1602; &#1575;&#1604;&#1589;&#1601;&#1581;&#1577;. &#1575;&#1604;&#1585;&#1580;&#1575;&#1569; &#1575;&#1604;&#1605;&#1581;&#1575;&#1608;&#1604;&#1577; &#1576;&#1593;&#1583; &#1602;&#1604;&#1610;&#1604;.",
            tts_servererror: "&#1581;&#1583;&#1579; &#1582;&#1591;&#1571; &#1601;&#1610; &#1575;&#1604;&#1582;&#1575;&#1583;&#1605;. &#1575;&#1604;&#1585;&#1580;&#1575;&#1569; &#1575;&#1604;&#1605;&#1581;&#1575;&#1608;&#1604;&#1577; &#1604;&#1575;&#1581;&#1602;&#1575;&#1611;.",
            tts_seconds: "&#1579;&#1608;&#1575;&#1606;&#1613;",
            tts_explain: "&#1604;&#1575;&#1587;&#1578;&#1582;&#1583;&#1575;&#1605; &#1582;&#1575;&#1589;&#1610;&#1577; &#1606;&#1591;&#1602; &#1575;&#1604;&#1606;&#1589;&#1548; &#1575;&#1604;&#1585;&#1580;&#1575;&#1569; &#1578;&#1581;&#1583;&#1610;&#1583; &#1575;&#1604;&#1606;&#1589; &#1575;&#1604;&#1605;&#1585;&#1575;&#1583; &#1578;&#1581;&#1608;&#1610;&#1604;&#1607; &#1593;&#1604;&#1609; &#1607;&#1584;&#1607; &#1575;&#1604;&#1589;&#1601;&#1581;&#1577;. &#1576;&#1593;&#1583; &#1584;&#1604;&#1603; &#1575;&#1590;&#1594;&#1591; &#1586;&#1585; &#1606;&#1591;&#1602; &#1575;&#1604;&#1606;&#1589;&#1548; &#1608;&#1575;&#1590;&#1594;&#1591; &#1582;&#1610;&#1575;&#1585; &quot;&#1575;&#1604;&#1606;&#1589; &#1575;&#1604;&#1605;&#1581;&#1583;&#1583;&quot;.",
            // tts_select_voice: "&#1602;&#1605; &#1576;&#1578;&#1592;&#1604;&#1610;&#1604; &#1575;&#1604;&#1606;&#1589; &#1608;&#1575;&#1582;&#1578;&#1610;&#1575;&#1585; &#1575;&#1604;&#1589;&#1608;&#1578;",
            tts_select_voice: "اختر صوتًا",
            tts_male: "&#1605;&#1584;&#1603;&#1585;",
            tts_female: "&#1605;&#1572;&#1606;&#1579;",
            tts_detected_language: "اللغة المكتشفة: ",
            tts_fetch_error: "حدثت مشكلة في عملية الجلب: ",
            tts_param_error: "TTS غير متوفر للمعلمات الحالية."
        });

        AtKit.addLocalisationMap("id", {
            tts_title: "Text to Speech",
            tts_ssapi_select_text: "Pilih beberapa teks dan tekan tombol TTS untuk membacanya.",
            tts_options: "Pilihan Text to Speech",
            tts_converting: "Konversi Text to Speech sedang berlangsung.",
            tts_timeremaining: "Waktu yang tersisa:",
            tts_pleasewait: "Mohon tunggu ...",
            tts_playpause: "Mainkan / Jeda",
            tts_rewind: "Putar ulang",
            tts_stop: "Hentikan & Tutup TTS",
            tts_error: "Kesalahan",
            tts_overloaded: "Server saat ini sedang kelebihan kapasitas untuk konversi Text to Speech. Silakan coba lagi nanti.",
            tts_problem: "Terjadi kesalahan saat kami mengonversi halaman ini menjadi ucapan. Silakan coba beberapa saat lagi.",
            tts_servererror: "Terjadi kesalahan pada server. Silakan coba lagi nanti.",
            tts_seconds: "detik",
            tts_explain: "Untuk menggunakan fitur Text to Speech dengan teks yang dipilih, pertama-tama pilih teks di halaman ini yang ingin Anda konversi. Setelah Anda melakukan ini, klik tombol Text to Speech, dan pilih opsi 'teks yang dipilih'.",
            tts_select_voice: "Pilih suara",
            tts_male: "Pria",
            tts_female: "Wanita",
            tts_detected_language: "Bahasa yang terdeteksi: ",
            tts_fetch_error: "Ada masalah dengan operasi pengambilan: ",
            tts_param_error: "TTS tidak tersedia untuk parameter ini."
        });

        TTSDialogs = {
            TTSSpeechSynthesisAPIRequiresSelection: {
                title: AtKit.localisation("tts_options"),
                body: AtKit.localisation("tts_ssapi_select_text")
            },
            options: {
                title: AtKit.localisation("tts_options"),
                body: AtKit.localisation("tts_detected_language") + "<span id=\"detected_language\"></span><br>" + AtKit.localisation("tts_select_voice") + " <br /><button id=\"sbStartInsipioTTSSelectionMale\"> " + AtKit.localisation("tts_male") + "</button> <button id=\"sbStartInsipioTTSSelectionFemale\"> " + AtKit.localisation("tts_female") + "</button>"
            },
            jssapi_options: {
                title: AtKit.localisation("tts_options"),
                body: AtKit.localisation("tts_detected_language") + "<span id=\"detected_language\"></span><br>" + AtKit.localisation("tts_select_voice") + "<div id='sbJssapiVoiceOptions'></div>"
            },
            starting: {
                title: AtKit.localisation("tts_title"),
                body: "<center>" + AtKit.localisation("tts_converting") + " <br /><img src='" + AtKit.getPluginURL() + "images/loadingbig.gif' /><br />" + AtKit.localisation("tts_timeremaining") + " <div id='sbttstimeremaining'>...</div><br />" + AtKit.localisation("tts_pleasewait") + " </center>"
            }
        };

        AtKit.addFn("getSelectedTextInsipioTTS", function (strip) {
            var text = AtKit.call("getSelectedTextInElementInsipio");

            if (text == null) {
                text = "";

                if (document.selection && document.selection.type !== "Control" && document.selection.createRange().text !== "") {
                    text = document.selection.createRange().text;
                } else if (window.getSelection && window.getSelection().toString() !== "") {
                    text = window.getSelection().toString();
                } else if (document.getSelection) {
                    text = document.getSelection();
                }
            }

            if (strip === true) {
                return String(text).replace(/([\s]+)/ig, "");
            }

            text = String(text);

            // get selection text from context menu (right click)
            if (text === "") {
                text = window.AtSelectionText;
                window.AtSelectionText = "";// empty the variable
            }

            if (text === "") {
                // copy to clipboard and get its content
                let successful;
                try {
                    successful = document.execCommand("copy");
                } catch (err) {
                    successful = false;
                }
                if (successful) {
                    // text = document.execCommand("paste");
                }
            }

            return text;
        });

        AtKit.addFn("getSelectedTextInElementInsipio", function () {
            var e = document.activeElement;

            return (

                /* mozilla / dom 3.0 */
                ("selectionStart" in e && function () {
                    var l = e.selectionEnd - e.selectionStart;
                    return e.value.substr(e.selectionStart, l);
                })

                /* exploder */
                || (document.selection && function () {
                    var nn = $lib(e).prop("nodeName");
                    var r = document.selection.createRange();
                    var re = e.createTextRange();
                    var rc = re.duplicate();

                    if (nn !== "input" && nn !== "textarea") return null;

                    e.focus();

                    if (r === null) {
                        return null;
                    }

                    re.moveToBookmark(r.getBookmark());
                    rc.setEndPoint("EndToStart", re);

                    return r.text;
                })

                /* browser not supported */
                || function () { return null; }

            )();
        });

        /**
         * Get setting from localStorage
         *
         * @param {string} key The attribut of localStorage
         * @return {Promise} The value of the requested setting
         */
        AtKit.addFn("TTSGetSetting", function (key) {
            return new Promise(function (resolve) {
                if (window.isExtension) {
                    if (window.extensionBrowser === "chrome") {
                        chrome.runtime.sendMessage({ message: "get_setting", key: key }, function (response) {
                            resolve(response.value);
                        });
                    } else if (window.extensionBrowser === "firefox") {
                        browser.runtime.sendMessage({ message: "get_setting", key: key }).then(function (response) {
                            resolve(response.value);
                        });
                    } else if (window.extensionBrowser === "edge") {
                        browser.runtime.sendMessage({ message: "get_setting", key: key }, function (response) {
                            resolve(response.value);
                        });
                    }
                } else {
                    resolve("");
                }
            });
        });

        /**
         * Checks client-side support for the Javascript Speech Synthesis API by probing for the fn.
         * Checks that a suitable client-side voice is available, given the lang of the page.
         *
         * @return {bool} Indicating whether JSSAPI is supported AND a suitable voice is available.
         */
        AtKit.addFn("TTSSpeechSynthesisAPICheckSupport", function (language) {
            let suitable_voices = [];
            if (Modernizr.speechsynthesis) {
                // get voices based on language
                suitable_voices = AtKit.call("TTSSpeechSynthesisFindVoiceForLanguage", { language: language });
            } else {
                // not-supported
                return false;
            }

            // Then check whether a voice is available for the current language
            return suitable_voices.length > 0;
        });

        /**
         * Identifies the language of the text in 'string'.
         *
         * @param {string} string The text sample we want to find the language of
         * @return {Promise} An ISO language code such as "en" or "fr".
         */
        AtKit.addFn("TTSSpeechSynthesisAPIGetStringLang", function (string) {
            let voice_language;

            return new Promise(function (resolve) {
                AtKit.call("TTSGetSetting", "AtTTSVoiceLanguage").then(function (value) {
                    if (value !== null && value !== undefined && value !== "") {
                        voice_language = value;
                    } else {
                        voice_language = "automatic";// default to "automatic"
                    }

                    if (window.isExtension) {
                        if (voice_language === "automatic") {
                            if (window.extensionBrowser === "chrome" && string !== "") {
                                // Detects the language of the provided text using CLD
                                chrome.i18n.detectLanguage(string, function (result) {
                                    if (result.languages.length > 0) {
                                        let detected_lang_percentage = 0;
                                        let detected_lang = window.AtKitLanguage;// page language will be used as default
                                        result.languages.forEach(function (element) {
                                            // we ignore reliablility and treat dominant language as the detected language
                                            if (element.percentage >= detected_lang_percentage && element.language !== "und" && element.language !== "un" && element.language !== "ut") {
                                                detected_lang_percentage = element.percentage;
                                                detected_lang = element.language;
                                            }
                                        });
                                        resolve(detected_lang);
                                    } else {
                                        resolve(window.AtKitLanguage);// default lang
                                    }
                                });
                            } else if (window.extensionBrowser === "firefox" && string !== "") {
                                // Detects the language of the provided text using CLD
                                browser.i18n.detectLanguage(string).then(function (result) {
                                    if (result.languages.length > 0) {
                                        let detected_lang_percentage = 0;
                                        let detected_lang = window.AtKitLanguage;// page language will be used as default
                                        result.languages.forEach(function (element) {
                                            // we ignore reliablility and treat dominant language as the detected language
                                            if (element.percentage >= detected_lang_percentage && element.language !== "und" && element.language !== "un" && element.language !== "ut") {
                                                detected_lang_percentage = element.percentage;
                                                detected_lang = element.language;
                                            }
                                        });
                                        resolve(detected_lang);
                                    } else {
                                        resolve(window.AtKitLanguage);// default lang
                                    }
                                });
                            } else if (window.extensionBrowser === "edge" && string !== "") {
                                // Edge doesn't support i18n.detectLanguage() so we use page language
                                resolve(window.AtKitLanguage);
                            } else {
                                resolve(window.AtKitLanguage);
                            }
                        } else {
                            resolve(voice_language);
                        }
                    } else if (/[\u0600-\u06FF]/.test(string)) {
                        // if the supplied text contains any Arabic
                        resolve("ar");
                    } else {
                        // Otherwise, return the page's lang, or 'en' if no lang is specified
                        resolve($lib("html").attr("lang") || "en");
                    }
                });
            });
        });

        /**
         * Prepares to read text using the Javascript Speech Synthesis API.
         * Enumerates suitable voices, shows the voice-selector dialog and populates the voice option buttons.
         *
         * @param {object} {text: "Text to be read", language: "Language of text"}
         * @return {null}
         */
        AtKit.addFn("TTSSpeechSynthesisAPIBeginReadText", function (args) {
            // Find a suitable voice
            var voices = AtKit.call("TTSSpeechSynthesisFindVoiceForLanguage", { language: args.language });
            // sort by voice name
            voices.sort(function (x, y) {
                var nameA = x.name;
                var nameB = y.name;
                if (nameA < nameB) {
                    return -1;
                }
                if (nameA > nameB) {
                    return 1;
                }
                // names must be equal
                return 0;
            });

            // Show the voices dialog box
            AtKit.show(TTSDialogs.jssapi_options);

            // set the detected language
            let detected_language = args.language;
            if (detected_language in cld_languages) {
                detected_language = cld_languages[detected_language];
            }
            $lib("span#detected_language").html(detected_language + ".");

            // facebox dialog element
            const voices_selector_dialog = $lib("#sbJssapiVoiceOptions").attr("style", "overflow-y: scroll; max-height: 200px; margin-top: 5px; border: 1px solid #EFEFEF; padding: 5px;");

            // For each voice, append a <button> to the voice-selector dialog
            $lib.each(voices, function (index, voice) {
                let name = voice.name;

                // Rejig RIDICULOUS names of MS voices to short names
                if (name.match(/Microsoft (.+) (?:Mobile)? - /) != null) name = name.match(/Microsoft (.+) (?:Mobile)? - /)[1];

                voices_selector_dialog.append($lib("<button>").html(name + "<br/><span style='color: gray'>" + voice.lang + "</span>").attr("data-voice-uri", voice.voiceURI).attr("style", "padding: 5px; margin: 0 5px 5px 0; min-width: 48%; border: 1px solid #AFAFAF; background-color: #ECECEC;")
                    .click(function () {
                        AtKit.call("TTSSpeechSynthesisAPIReadText", { text: args.text, voiceURI: $lib(this).attr("data-voice-uri") });
                        AtKit.hideDialog();// close dialog?
                    }));
            });
        });

        /**
         * Reads text using the Javascript Speech Synthesis API, and the specified voice.
         *
         * @param {array} args {text: the text to read, voiceURI: the URI of the JSSAPI voice to use}
         * @return {SpeechSynthesisUtterance} The SpeechSynthesisUtterance object that is speaking the text
         */
        AtKit.addFn("TTSSpeechSynthesisAPIReadText", function (args) {
            var utterance;
            var temp_utterance = [];
            var utterance_timer;
            var allow_pause_resume_timer = true;

            // Find the requested voice
            var voice;
            var voices = window.speechSynthesis.getVoices();
            for (let i = 0; i < voices.length; i += 1) {
                if (voices[i].voiceURI === args.voiceURI) {
                    voice = voices[i];
                    break;
                }
            }

            if (utterance_timer !== undefined && utterance_timer !== null) {
                clearInterval(utterance_timer);
            }

            utterance = new SpeechSynthesisUtterance(args.text);
            utterance.voice = voice;

            AtKit.call("TTSGetSetting", "AtTTSSplitLongText").then(function (value) {
                if (value !== null && value !== undefined) {
                    split_long_text = Boolean(value === "true");
                } else {
                    split_long_text = true;// default to "true"
                }

                utterance.onstart = function () {
                    AtKit.call("addTTSControl");

                    $lib("div#sbar").on("click", "#at-lnk-TTSPausePlay", function () {
                        const control_state = $lib("#at-lnk-TTSPausePlay").data("control-state");
                        if (control_state === "pause") {
                            window.speechSynthesis.pause();
                            allow_pause_resume_timer = false;

                            // remove HTML5 audio element to prevent conflict with SpeechSynthesis
                            audio = document.getElementById("audio_tts");
                            if (audio !== null) {
                                audio.remove();
                            }
                        } else {
                            window.speechSynthesis.resume();
                            allow_pause_resume_timer = true;
                        }

                        // unfocus the clicked link
                        this.blur();
                    });
                };
                utterance.onend = function () {
                    AtKit.call("removeTTSControl");

                    clearInterval(utterance_timer);
                    temp_utterance = [];// clean up
                };

                // IMPORTANT!! Do not remove: Logging the object out fixes some onend firing issues.
                // =================================================================================
                // console.log(utterance);
                temp_utterance.push(utterance);// instead of logging out the utterance in the console, we can temporarily store it in a variable
                // =================================================================================

                window.speechSynthesis.cancel();
                window.speechSynthesis.speak(utterance);
                window.speechSynthesis.resume();

                // Chrome browser has a bug when speaking utterance on long text (over 15 seconds), but .pause() and .resume() do the trick
                // Instead of chunking the long text, we'll pause and resume in timer
                if (split_long_text) {
                    utterance_timer = setInterval(function () {
                        if (allow_pause_resume_timer) {
                            window.speechSynthesis.pause();

                            // put the resume call in a timer solves pause-resume problem in Firefox
                            setTimeout(function () {
                                window.speechSynthesis.resume();
                            }, 1);// wait for 1 ms
                        }
                    }, 14000);// every 14 seconds
                }
            });

            return utterance;
        });

        /**
         * Finds and returns Javascript Speech Synthesis API voices that are suitable for the specified language.
         *
         * @param {object} args {language: the required language}
         * @return {array} best_voices Voices that speak in args.language
         */
        AtKit.addFn("TTSSpeechSynthesisFindVoiceForLanguage", function (args) {
            // Array to store suitable voices. This is returned at the end of the function.
            var best_voices = [];

            // Grab all the available voices.
            var voices = window.speechSynthesis.getVoices();

            // We lowercase everything prior to comparison
            args.language = args.language.toLowerCase();

            // First, look for an exact match (e.g. 'en-gb' text must have an 'en-gb' voice).
            $lib.each(voices, function (index, voice) {
                // If we have an exact match (e.g. 'en-gb' for 'en-gb'), put the voice in the output array.
                if (args.language === voice.lang.toLowerCase()) {
                    best_voices.push(voice);
                }
            });

            // Second, look for a match only on the top-level language (e.g. 'en-us' will do for 'en').
            $lib.each(voices, function (index, voice) {
                // If we have a language-only match (e.g. 'en-us' for 'en-gb'), put the voice in the output array
                // Also checks that the voice isn't an exact match, so we don't duplicate voices in best_voices.
                if (args.language !== voice.lang.toLowerCase() && args.language.substr(0, 2) === voice.lang.toLowerCase().substr(0, 2)) {
                    best_voices.push(voice);
                }
            });

            return best_voices;
        });

        AtKit.addFn("b64", function (input) {
            // + == _
            // / == -
            var bkeys = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_-=";
            var output = "";
            var chr1;
            var chr2;
            var chr3;
            var enc1;
            var enc2;
            var enc3;
            var enc4;
            var i = 0;

            input = AtKit.call("utf8_encode", input);

            while (i < input.length) {
                // chr1 = input.charCodeAt(i++);
                chr1 = input.charCodeAt(i);
                i += 1;

                // chr2 = input.charCodeAt(i++);
                chr2 = input.charCodeAt(i);
                i += 1;

                // chr3 = input.charCodeAt(i++);
                chr3 = input.charCodeAt(i);
                i += 1;

                enc1 = chr1 >> 2;
                enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
                enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
                enc4 = chr3 & 63;

                // if (isNaN(chr2)) {
                if (Number.isNaN(chr2)) {
                    enc3 = 64;
                    enc4 = enc3;
                // } else if (isNaN(chr3)) {
                } else if (Number.isNaN(chr3)) {
                    enc4 = 64;
                }

                output = output
                + bkeys.charAt(enc1) + bkeys.charAt(enc2)
                + bkeys.charAt(enc3) + bkeys.charAt(enc4);
            }

            return output;
        });

        AtKit.addFn("utf8_encode", function (string) {
            var utftext = "";
            string = string.replace(/\r\n/g, "\n");

            for (let n = 0; n < string.length; n += 1) {
                const c = string.charCodeAt(n);

                if (c < 128) {
                    utftext += String.fromCharCode(c);
                } else if ((c > 127) && (c < 2048)) {
                    utftext += String.fromCharCode((c >> 6) | 192);
                    utftext += String.fromCharCode((c & 63) | 128);
                } else {
                    utftext += String.fromCharCode((c >> 12) | 224);
                    utftext += String.fromCharCode(((c >> 6) & 63) | 128);
                    utftext += String.fromCharCode((c & 63) | 128);
                }
            }

            return utftext;
        });

        AtKit.addFn("sendInsipioTTSChunk", function (args) {
            var start;
            var endPoint;
            var payload;
            var urlString;
            if (args.block === 1) {
                start = 0;
            } else {
                start = (settings.ttsChunkSize * args.block);
            }

            if ((start + settings.ttsChunkSize) > args.fullData.length) {
                endPoint = args.fullData.length;
            } else {
                endPoint = (start + settings.ttsChunkSize);
            }

            payload = args.fullData.substring(start, endPoint);

            urlString = settings.speechServicesURL + "insipio-tts/request.php?rt=tts&v=2&i=1&l=" + AtKit.getLanguage() + "&voice=" + args.voice + "&id=" + args.reqID + "&data=" + payload + "&chunkData=" + args.totalBlocks + "-" + args.block;
            if (args.block === args.totalBlocks - 1) {
                urlString += "&page=" + encodeURIComponent(window.location);
            }

            if (window.isExtension) {
                // ommit the "?"
                urlString += "&callback=";

                // using browser extension's cross-origin AJAX, instead of JSONP
                $lib.ajax({
                    url: urlString,
                    dataType: "text",
                    success: function (RO) {
                        var errorTitle = "<h2>" + AtKit.localisation("tts_error") + "</h2>";

                        // trim & parse returned JSON string
                        RO = RO.substring(1);// trim left "("
                        RO = RO.substring(0, RO.length - 2);// trim right ");"
                        RO = JSON.parse(RO);

                        $lib("#compactStatus").html(args.block + " / " + args.totalBlocks);

                        if (args.block === args.totalBlocks) {
                            // Finished request..
                            AtKit.show(TTSDialogs.starting);
                            if (RO.status === "encoding") {
                                AtKit.call("countdownInsipioTTS", { timeLeft: (RO.est_completion / RO.chunks), id: RO.ID });
                            } else if (RO.status === "failure" && RO.reason === "overcapacity") {
                                AtKit.message(errorTitle + "<p>" + AtKit.localisation("tts_overloaded") + "</p>");
                            } else if (RO.status === "failure" && RO.message === "") {
                                AtKit.message(errorTitle + "<p>" + AtKit.localisation("tts_problem") + "</p>");
                            } else {
                                AtKit.message(errorTitle + "<p>" + RO.reason + " " + RO.data.message + "</p>");
                            }
                        } else if (RO.data.message === "ChunkSaved") { // Send the next block.
                            AtKit.call("sendInsipioTTSChunk", {
                                fullData: args.fullData,
                                block: (args.block + 1),
                                totalBlocks: args.totalBlocks,
                                reqID: args.reqID
                            });
                        } else {
                            AtKit.message(errorTitle + "<p>" + AtKit.localisation("tts_servererror") + "</p>");
                        }
                    }
                });
            } else {
                urlString += "&callback=?";

                $lib.getJSON(urlString, function (RO) {
                    var errorTitle = "<h2>" + AtKit.localisation("tts_error") + "</h2>";
                    $lib("#compactStatus").html(args.block + " / " + args.totalBlocks);

                    if (args.block === args.totalBlocks) {
                        // Finished request..
                        AtKit.show(TTSDialogs.starting);
                        if (RO.status === "encoding") {
                            AtKit.call("countdownInsipioTTS", { timeLeft: (RO.est_completion / RO.chunks), id: RO.ID });
                        } else if (RO.status === "failure" && RO.reason === "overcapacity") {
                            AtKit.message(errorTitle + "<p>" + AtKit.localisation("tts_overloaded") + "</p>");
                        } else if (RO.status === "failure" && RO.message === "") {
                            AtKit.message(errorTitle + "<p>" + AtKit.localisation("tts_problem") + "</p>");
                        } else {
                            AtKit.message(errorTitle + "<p>" + RO.reason + " " + RO.data.message + "</p>");
                        }
                    } else if (RO.data.message === "ChunkSaved") { // Send the next block.
                        AtKit.call("sendInsipioTTSChunk", {
                            fullData: args.fullData, block: (args.block + 1), totalBlocks: args.totalBlocks, reqID: args.reqID
                        });
                    } else {
                        AtKit.message(errorTitle + "<p>" + AtKit.localisation("tts_servererror") + "</p>");
                    }
                });
            }
        });

        AtKit.addFn("countdownInsipioTTS", function (arg) {
            var audioContainer = "audioo";
            var chunkUrls = [];
            // if (isNaN(arg.timeLeft)) {
            if (Number.isNaN(arg.timeLeft)) {
                AtKit.message("<h2>" + AtKit.localisation("tts_error") + "</h2> <p>" + AtKit.localisation("tts_problem") + "</p>");
            } else if (arg.timeLeft === 0) {
                // Play audio
                if (!$lib.browser.msie) { // Deprecated, will not work in jQuery 1.9 or later unless the jQuery Migrate plugin is included.
                    // if other than Microsoft's Internet Explorer
                    if (Modernizr.audio) { // if the browser supports playing audio (HTML5)
                    // a = document.createElement("audio");
                    // if (a !== undefined && a.canPlayType("audio/mpeg") !== "") { // if the browser supports playing audio (HTML5)
                        if (window.isExtension) {
                            // using browser extension's cross-origin AJAX, instead of JSONP
                            // ommit the "?", so server will treat empty string as callback name
                            $lib.ajax({
                                url: settings.speechServicesURL + "cache/request.php?id=" + arg.id + "&callback=",
                                dataType: "text",
                                success: function (jsonDocument) {
                                    // trim & parse returned JSON string
                                    jsonDocument = jsonDocument.substring(1);// trim left "("
                                    jsonDocument = jsonDocument.substring(0, jsonDocument.length - 2);// trim right ");"
                                    jsonDocument = JSON.parse(jsonDocument);

                                    // const index = 0;
                                    if ($lib.isArray(jsonDocument.trackList.track)) { // Store the audio urls to be played later by the audio html5 element
                                        $lib.each(jsonDocument.trackList.track, function (key, value) {
                                            const audioURL = value.location;
                                            chunkUrls.push(audioURL);
                                        });
                                    } else {
                                        chunkUrls.push(jsonDocument.trackList.track.location);
                                    }

                                    AtKit.call("playChunksHtml5", { chunkUrls: chunkUrls, index: 0 });
                                }
                            });
                        } else {
                            $lib.getJSON(settings.speechServicesURL + "cache/request.php?id=" + arg.id + "&callback=?")
                                .done(function (jsonDocument) {
                                    // const index = 0;
                                    if ($lib.isArray(jsonDocument.trackList.track)) { // Store the audio urls to be played later by the audio html5 element
                                        $lib.each(jsonDocument.trackList.track, function (key, value) {
                                            const audioURL = value.location;
                                            chunkUrls.push(audioURL);
                                        });
                                    } else {
                                        chunkUrls.push(jsonDocument.trackList.track.location);
                                    }

                                    AtKit.call("playChunksHtml5", { chunkUrls: chunkUrls, index: 0 });
                                });
                        }
                    } else {
                        $lib("body").append($lib("<div id=\"flashContent\"><OBJECT classid=\"clsid:D27CDB6E-AE6D-11cf-96B8-444553540000\" codebase=\"http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,40,0\" width=\"1\" height=\"1\" id=\"audioe\"> <PARAM name=movie value=\"" + settings.speechServicesURL + "lib/player/player-licensed.swf\"></PARAM> <PARAM name=flashvars value=\"file=" + settings.speechServicesURL + "cache/" + arg.id + ".xml&autostart=true&playlist=bottom&repeat=list&playerready=playerReady&id=" + audioContainer + "\"><PARAM name=allowscriptaccess value=\"always\" /><embed type=\"application/x-shockwave-flash\" pluginspage=\"http://www.macromedia.com/go/getflashplayer\" src=\"" + settings.speechServicesURL + "lib/player/player-licensed.swf\" width=\"1\" height=\"1\" allowscriptaccess=\"always\" allowfullscreen=\"false\" flashvars=\"file=" + settings.speechServicesURL + "cache/" + arg.id + ".xml&autostart=true&playlist=bottom&repeat=list&playerready=playerReady\" name=\"audioe\" /> </OBJECT></div>"));

                        AtKit.call("setupInsipioTTSListeners");
                    }
                } else { // if Microsoft's Internet Explorer
                    $lib("<div />", { id: "flashContent" }).prependTo("body");

                    const params = {
                        flashvars: "file=" + settings.speechServicesURL + "cache/" + arg.id + ".xml&autostart=true&playlist=bottom&repeat=list&playerready=playerReady&id=" + audioContainer,
                        allowscriptaccess: "always"
                    };
                    const attributes = {
                        id: audioContainer,
                        name: audioContainer
                    };

                    if (typeof swfobject !== "undefined") {
                        swfobject.embedSWF(settings.speechServicesURL + "lib/player/player-licensed.swf", "flashContent", "1", "1", "9.0.0", "expressInstall.swf", false, params, attributes, function () {
                            AtKit.call("setupInsipioTTSListeners");
                        });
                    }
                }

                AtKit.hideDialog();
            } else {
                $lib("#sbttstimeremaining").html(arg.timeLeft + " " + AtKit.localisation("tts_seconds"));
                window.setTimeout(function () { AtKit.call("countdownInsipioTTS", { timeLeft: (arg.timeLeft - 1), id: arg.id }); }, 1000);
            }
        });

        AtKit.addFn("setupInsipioTTSListeners", function () {
            if (AtKit.get("TTS_Listeners_setup") === true) return;

            window.playerReady = function (obj) {
                AtKit.set("ATAudioPlayerID", obj.id);

                /*
                for(b in TTSExtendedObject.TTSButtons){
                    var obj = TTSExtendedObject.TTSButtons[b];
                    AtKit.addButton(b, obj.tooltip, obj.icon, obj.fn);
                } */

                // Set values.
                AtKit.set("TTS_position", 0);
                AtKit.set("TTS_playingItem", 0);

                // Add page listeners
                if (typeof swfobject !== "undefined") {
                    let playerObj = swfobject.getObjectById(obj.id);

                    if ($lib.browser !== "msie") {
                        playerObj = window.document.audioe;
                    }

                    playerObj.addModelListener("STATE", "ATBarAudioStateListener");
                    playerObj.addModelListener("TIME", "ATBarAudioTimeMonitor");
                    playerObj.addControllerListener("ITEM", "ATBarAudioItemMonitor");
                }
            };

            window.ATBarAudioTimeMonitor = function (obj) {
                AtKit.set("TTS_position", obj.position);
            };

            window.ATBarAudioItemMonitor = function (obj) {
                AtKit.set("TTS_playingItem", obj.index);
            };

            window.ATBarAudioStateListener = function (obj) {
                var state = obj.newstate;

                if (typeof swfobject !== "undefined") {
                    let playerObj = swfobject.getObjectById(obj.id);

                    if ($lib.browser !== "msie") {
                        playerObj = window.document.audioe;
                    }

                    if (state === "COMPLETED" && (AtKit.get("TTS_playingItem") + 1) === playerObj.getPlaylist().length) {
                        // Completed, remove controlbox and reset everything back to normal.
                        AtKit.call("TTSRemoveControlBox");
                    }
                }

                if (state === "IDLE" || state === "PAUSED") {
                    $lib("#at-lnk-ttsPlay").children("img").attr("src", AtKit.getPluginURL() + "images/control.png");
                    $lib("#at-btn-tts").children("img").attr("src", AtKit.getPluginURL() + "images/sound.png").css("padding-top", "6px");
                } else if (AtKit.get("TTS_clickEnabled") === false) {
                    $lib("#at-lnk-ttsPlay").children("img").attr("src", AtKit.getPluginURL() + "images/control-pause.png");
                    $lib("#at-btn-tts").children("img").attr("src", AtKit.getPluginURL() + "images/loading.gif").css("padding-top", "8px");
                }
            };

            AtKit.set("TTS_Listeners_setup", true);
        });

        AtKit.addFn("TTSRemoveControlBox", function () {
            AtKit.removeButton("ttsPlay");
            AtKit.removeButton("ttsRewind");
            AtKit.removeButton("ttsStop");

            $lib("#flashContent").remove();
            $lib("#at-lnk-tts").children("img").attr("src", AtKit.getPluginURL() + "images/sound.png").css("padding-top", "6px");
            AtKit.set("TTS_clickEnabled", true);
        });

        AtKit.addFn("multipartText", function (args) {
            const multipartText = [];
            const text = args.text;
            let CHARACTER_LIMIT = args.char_limit;

            if (CHARACTER_LIMIT === undefined) {
                CHARACTER_LIMIT = 200;// 200 characters per chunk
            }
            if (text.length > CHARACTER_LIMIT) {
                let tmp_txt = text;
                while (tmp_txt.length > CHARACTER_LIMIT) {
                    let p = -1;// position of delimiter
                    let pos;
                    let result;
                    let part = "";

                    // split by common phrase delimiters
                    let re = /(\.[^0-9]+|[:!¡?¿;()[\]—«»\n]+)/g;
                    result = re.exec(tmp_txt);
                    pos = -1;
                    if (result !== null) {
                        pos = result.index;
                    }
                    while (result !== null && pos < CHARACTER_LIMIT) {
                        p = pos;
                        result = re.exec(tmp_txt);
                        if (result !== null) {
                            pos = result.index;
                        }
                    }

                    // couldn't split by priority characters, try commas
                    if (p === -1 || p >= CHARACTER_LIMIT) {
                        re = /,[^0-9]/g;
                        result = re.exec(tmp_txt);
                        pos = -1;
                        if (result !== null) {
                            pos = result.index;
                        }
                        while (result !== null && pos < CHARACTER_LIMIT) {
                            p = pos;
                            result = re.exec(tmp_txt);
                            if (result !== null) {
                                pos = result.index;
                            }
                        }
                    }

                    // couldn't split by normal characters, then we use spaces as our last resort
                    if (p === -1 || p >= CHARACTER_LIMIT) {
                        // check for spaces. If no spaces then split by CHARACTER_LIMIT characters.
                        if (tmp_txt.search(" ") === -1) {
                            part = tmp_txt.substr(0, CHARACTER_LIMIT);
                        } else {
                            const words = tmp_txt.split(" ");
                            for (let i = 0; i < words.length; i += 1) {
                                if (part.length + words[i].length + 1 > CHARACTER_LIMIT) {
                                    break;
                                }
                                part += (i !== 0 ? " " : "") + words[i];
                            }
                        }
                    } else {
                        part = tmp_txt.substr(0, p + 1);
                    }

                    tmp_txt = tmp_txt.substr(part.length, tmp_txt.length - part.length);

                    // remove the first single character of common phrase delimiters
                    // part = part.replace(/(^[:!¡?¿;()[\]—«»\n.])?/, "");

                    // add into multipartText
                    part = part.trim();
                    if (part.length > 0) {
                        multipartText.push(part);
                    }
                }

                // remove the first single character of common phrase delimiters
                // tmp_txt = tmp_txt.replace(/(^[:!¡?¿;()[\]—«»\n.])?/, "");

                // add the remaining part into multipartText
                tmp_txt = tmp_txt.trim();
                if (tmp_txt.length > 0) {
                    multipartText.push(tmp_txt);
                }
            } else {
                // small text, no need to split
                multipartText.push(text);
            }

            return multipartText;
        });

        AtKit.addFn("sbStartInsipioTTSSelection", function (args) {
            selectedData = args.text;
            selectedData = selectedData.replace(/\r\n/g, "\n");// replace \r\n with \n, responsivevoice has problem parsing the URI

            if (typeof selectedData !== "undefined" && selectedData !== "") {
                // get "TTS Service" setting
                AtKit.call("TTSGetSetting", "AtTTSService").then(function (tts_service_value) {
                    let tts_service = tts_service_value;// "responsivevoice", "googletts", "insipio"
                    if (tts_service_value === null || tts_service_value === undefined || tts_service_value === "") {
                        tts_service = "responsivevoice"; // default
                    }

                    if (tts_service === "insipio") {
                        // the code below uses Insipio TTS service from https://speech.services.atbar.org/
                        // ===============================================================================
                        // Send the data in chunks, as chances are we cant get it all into one request.
                        const transmitData = AtKit.call("b64", selectedData);

                        const chunks = Math.ceil(transmitData.length / settings.ttsChunkSize);

                        if (chunks > 0) {
                            const reqID = Math.floor(Math.random() * 5001);

                            AtKit.message("<h2>" + AtKit.localisation("tts_pleasewait") + "</h2><p>" + AtKit.localisation("tts_converting") + "...<br /><div id='compactStatus'>0 / " + chunks + "</div></p>");

                            AtKit.call("sendInsipioTTSChunk", {
                                fullData: transmitData, block: 1, totalBlocks: chunks, reqID: reqID, voice: args.voice
                            });
                        } else {
                            AtKit.message("<h2>" + AtKit.localisation("tts_error") + "</h2><p>" + AtKit.localisation("tts_problem") + "</p>");
                        }
                        // ===============================================================================
                    } else {
                        // first, we get the detected language of the string
                        AtKit.call("TTSSpeechSynthesisAPIGetStringLang", selectedData)
                            .then(function (detected_lang) {
                                let lang = detected_lang;
                                const gender = args.voice;

                                // get "Split Long Text" setting
                                AtKit.call("TTSGetSetting", "AtTTSSplitLongText").then(function (value) {
                                    if (value !== null && value !== undefined) {
                                        split_long_text = Boolean(value === "true");
                                    } else {
                                        split_long_text = true;// default to true
                                    }

                                    let multipartText = [];
                                    if (split_long_text) {
                                        multipartText = AtKit.call("multipartText", { text: selectedData, char_limit: 200 });
                                    }

                                    // the detected language doesn't always have voice
                                    const supported_languages = ["af", "sq", "ar", "hy", "bn", "bs", "bg", "ca", "zh", "zh-CN", "zh-TW", "hr", "cs", "da", "nl", "en", "eo", "et", "fi", "fr", "ka", "de", "el", "hi", "hu", "is", "id", "it", "ja", "jv", "jw", "kn", "km", "ko", "ku", "la", "lv", "mk", "ml", "mr", "mo", "sr-ME", "ne", "nb", "no", "pl", "pt", "pt-BR", "pt-PT", "ro", "ru", "sr", "sh", "si", "sk", "es", "su", "sw", "sv", "fil", "ta", "te", "th", "tr", "uk", "vi", "cy"];

                                    // match lang with available voice collections from ResponsiveVoice / Google Translate
                                    if (lang === "nn") {
                                        lang = "nb";// Norwegian
                                    } else if (lang === "ms") {
                                        lang = "id";// use Indonesian voice for Malaysian
                                    } else if (supported_languages.indexOf(lang) === -1) {
                                        lang = "en-US";// use US English as default voice
                                    }

                                    const chunkUrls = [];
                                    if (tts_service === "responsivevoice") {
                                        // we're using TTS service from ResponsiveVoice JS v1.5.8
                                        // ======================================================
                                        // const unsupported_responsivevoice = ["tr", "hi", "vi"];
                                        let service = "g1";
                                        if (lang === "la" || lang === "mo" || lang === "sh" || lang === "mr" || lang === "te") {
                                            service = "g2";// fallback service for Latin, Moldavian, Serbo Croatian, Marathi, Telugu
                                        }
                                        if (lang === "jw") {
                                            lang = "jv";// Javanese
                                        }

                                        const voicename = "";
                                        const pitch = "0.5";
                                        const rate = "0.4";
                                        const volume = "1";

                                        // ResponsiveVoice only allows < 4096 characters per request
                                        if (selectedData.length >= 4096) {
                                            split_long_text = true;
                                            multipartText = AtKit.call("multipartText", { text: selectedData, char_limit: 200 });
                                        }

                                        if (!split_long_text) {
                                            // if we want to send the text all at once, use this, otherwise proceed with the chunks
                                            const responsiveVoiceURL = "https://code.responsivevoice.org/getvoice.php?t=" + encodeURIComponent(selectedData) + "&tl=" + lang + "&sv=" + service + "&vn=" + voicename + "&pitch=" + pitch + "&rate=" + rate + "&vol=" + volume + "&gender=" + gender;
                                            chunkUrls.push(responsiveVoiceURL);
                                        } else {
                                            for (let i = 0; i < multipartText.length; i += 1) {
                                                // ResponsiveVoice JS seems to use Google TTS service
                                                const responsiveVoiceURL = "https://code.responsivevoice.org/getvoice.php?t=" + encodeURIComponent(multipartText[i]) + "&tl=" + lang + "&sv=" + service + "&vn=" + voicename + "&pitch=" + pitch + "&rate=" + rate + "&vol=" + volume + "&gender=" + gender;
                                                chunkUrls.push(responsiveVoiceURL);
                                            }
                                        }
                                    } else if (tts_service === "googletts") {
                                        // as an alternative when there's a problem with ResponsiveVoice server, we can use Google Translate TTS, but no guarantee this will always work (Google may block it or change the API), and we can't use the gender preference
                                        if (tts_service === "googletts") {
                                            split_long_text = true;// Google Translate only allows limited chars per request
                                        }
                                        if (split_long_text) {
                                            multipartText = AtKit.call("multipartText", { text: selectedData, char_limit: 200 });
                                        }

                                        // const unsupported_googletts = ["bg", "kn", "ka", "ku"];

                                        // We use different Google Translate domain for each request, hopefully this can avoid blocking. Please provide the Google Translate hostname as many as possible.
                                        const arr_google_translate_domain = ["https://translate.google.com", "https://translate.google.co.uk", "https://translate.google.co.id"];
                                        for (let i = 0; i < multipartText.length; i += 1) {
                                            let google_translate_domain;
                                            if (i < arr_google_translate_domain.length) {
                                                google_translate_domain = arr_google_translate_domain[i];
                                            } else {
                                                google_translate_domain = arr_google_translate_domain[i % arr_google_translate_domain.length];
                                            }
                                            const googleTranslateTTSURL = google_translate_domain + "/translate_tts?ie=UTF-8&q=" + encodeURIComponent(multipartText[i]) + "&tl=" + lang + "&client=tw-ob";
                                            chunkUrls.push(googleTranslateTTSURL);
                                        }
                                    }

                                    // play each chunk
                                    AtKit.call("playChunksHtml5", { chunkUrls: chunkUrls, index: 0 });
                                });
                            });
                    }
                });
            } else {
                AtKit.message("<h2>" + AtKit.localisation("tts_title") + "</h2><p>" + AtKit.localisation("tts_explain") + "</p>");
            }
        });

        /**
         * Add TTS Control buttons on the toolbar
         */
        AtKit.addFn("addTTSControl", function () {
            // first, remove existing TTS Control
            // AtKit.call("removeTTSControl");

            // ignore if the buttons have already been created
            if (document.getElementById("at-lnk-TTSPausePlay") != null) {
                return;
            }

            AtKit.addSeparator("TTSControl");
            AtKit.addButton("TTSPausePlay",
                AtKit.localisation("tts_playpause"),
                AtKit.getPluginURL() + "images/control-pause.png",
                function () {
                    // change icon based on control state
                    let control_state = $lib("#at-lnk-TTSPausePlay").data("control-state");
                    if (control_state === undefined || control_state === "resume") {
                        control_state = "pause";
                        $lib("#at-lnk-TTSPausePlay img").attr("src", AtKit.getPluginURL() + "images/control.png");

                        // pause HTML5 audio playback
                        if (playPromise !== undefined && playPromise !== null) {
                            playPromise
                                .then(function () {
                                    audio = document.getElementById("audio_tts");
                                    if (audio !== undefined && audio !== null && !audio.paused) {
                                        audio.pause();
                                    }
                                });
                        }
                    } else {
                        control_state = "resume";
                        $lib("#at-lnk-TTSPausePlay img").attr("src", AtKit.getPluginURL() + "images/control-pause.png");

                        // resume HTML5 audio playback
                        audio = document.getElementById("audio_tts");
                        if (audio !== undefined && audio !== null && audio.paused) {
                            playPromise = audio.play();
                        }
                    }
                    $lib("#at-lnk-TTSPausePlay").data("control-state", control_state);
                });
            AtKit.addButton("TTSStop",
                AtKit.localisation("tts_stop"),
                AtKit.getPluginURL() + "images/control-stop-square.png",
                function () {
                    // cancel speaking utterance
                    const synth = window.speechSynthesis;
                    if (synth.speaking) {
                        synth.cancel();
                    }

                    // stop the HTML5 audio playback
                    if (playPromise !== undefined && playPromise !== null) {
                        playPromise
                            .then(function () {
                                audio = document.getElementById("audio_tts");
                                if (audio !== undefined && audio !== null) {
                                    audio.pause();

                                    // cleanup
                                    window.sessionStorage.clear();
                                    playPromise = null;
                                }
                            });
                    }

                    // remove control buttons
                    AtKit.call("removeTTSControl");
                });
        });

        /**
         * Remove TTS Control buttons
         */
        AtKit.addFn("removeTTSControl", function () {
            AtKit.removeSeparator("TTSControl");
            AtKit.removeButton("TTSPausePlay");
            AtKit.removeButton("TTSStop");

            // remove tooltip
            $lib("body > div.tooltip").fadeOut(400, function () {
                $lib(this).remove();
            });
        });

        /**
         * Fetch the TTS audio and save into sessionStorage for later playback
         */
        AtKit.addFn("fetchAndCacheTTSAudio", function (args) {
            var tts_audio = {};

            const chunkUrls = args.chunkUrls;
            const index = args.index;

            // check if chunkUrls[index] exists in sessionStorage
            const tts_audio_str = window.sessionStorage.getItem("AtTTSAudio");
            if (tts_audio_str !== null) {
                tts_audio = JSON.parse(tts_audio_str);// object of saved audio per URL (Blob)
            }

            const CACHE_NUMBER = 3;
            for (let i = index; i < Math.min(chunkUrls.length, index + CACHE_NUMBER); i += 1) {
                if (!(chunkUrls[i] in tts_audio)) {
                    // fetch TTS audio and save into sessionStorage
                    const fetch_option = {
                        mode: "cors",
                        cache: "default",
                        redirect: "follow",
                        referrerPolicy: "no-referrer"
                    };
                    fetch(chunkUrls[i], fetch_option)
                        .then(function (response) {
                            if (response.ok) {
                                return response.blob();
                            }
                            throw new Error(AtKit.localisation("tts_param_error"));
                        })
                        .then(function (blob) {
                            const fileReader = new FileReader();
                            fileReader.onload = function (event) {
                                // Read out file contents as a Data URL
                                tts_audio[chunkUrls[i]] = event.target.result;

                                // save into sessionStorage
                                window.sessionStorage.setItem("AtTTSAudio", JSON.stringify(tts_audio));
                            };
                            // Load blob as Data URL
                            fileReader.readAsDataURL(blob);
                        })
                        .catch(function (error) {
                            console.log("There has been a problem with the fetch operation: " + error.message);
                        });
                }
            }
        });

        // Recursive function to play the mp3 files using the audio html5 element
        AtKit.addFn("playChunksHtml5", function (args) {
            var tts_audio = {};

            const chunkUrls = args.chunkUrls;
            const index = args.index;

            const tts_audio_str = window.sessionStorage.getItem("AtTTSAudio");
            if (tts_audio_str !== null) {
                tts_audio = JSON.parse(tts_audio_str);// object of saved audio per URL (Blob)
            }

            audio = document.getElementById("audio_tts");
            if (audio === undefined || audio === null) {
                audio = document.createElement("audio");
                audio.id = "audio_tts";
                // insert newly created audio element into DOM
                document.body.appendChild(audio);
            }

            if (index < chunkUrls.length) {
                // delete the previously played audio cache from sessionStorage
                if (index - 1 > -1 && chunkUrls[index - 1] in tts_audio) {
                    delete tts_audio[chunkUrls[index - 1]];
                    window.sessionStorage.setItem("AtTTSAudio", JSON.stringify(tts_audio));
                }

                if (chunkUrls[index] in tts_audio) {
                    // load from sessionStorage & play the audio
                    audio.src = tts_audio[chunkUrls[index]];
                    audio.load();

                    playPromise = audio.play();
                    playPromise
                        .then(function () {
                            // if playback succeeds, then add TTS control
                            if (index === 0) {
                                // add TTS control buttons
                                AtKit.call("addTTSControl");

                                // unfocus the clicked link
                                $lib("div#sbar").on("click", "#at-lnk-TTSPausePlay", function () {
                                    this.blur();
                                });
                            }

                            // close dialog
                            AtKit.hideDialog();

                            // fetch & cache next TTS audio
                            if ((index + 1) < chunkUrls.length) {
                                AtKit.call("fetchAndCacheTTSAudio", { chunkUrls: chunkUrls, index: index + 1 });
                            }
                        })
                        .catch(function (error) {
                            AtKit.message("<h2>" + AtKit.localisation("tts_error") + "</h2><p>" + error + "</p>");

                            // remove TTS control buttons
                            AtKit.call("removeTTSControl");
                        });
                } else {
                    // show loading dialog
                    AtKit.message("<h2>" + AtKit.localisation("tts_title") + "</h2><center><img src='" + AtKit.getPluginURL() + "images/loadingbig.gif' /></center>");

                    // fetch the (first chunk of) audio file and then play it
                    const fetch_option = {
                        mode: "cors",
                        cache: "default",
                        redirect: "follow",
                        referrerPolicy: "no-referrer"
                    };
                    fetch(chunkUrls[index], fetch_option)
                        .then(function (response) {
                            if (response.ok) {
                                return response.blob();
                            }
                            throw new Error(AtKit.localisation("tts_param_error"));
                        })
                        .then(function (blob) {
                            const fileReader = new FileReader();
                            fileReader.onload = function (event) {
                                // Read out file contents as a Data URL
                                tts_audio[chunkUrls[index]] = event.target.result;

                                // save into sessionStorage
                                window.sessionStorage.setItem("AtTTSAudio", JSON.stringify(tts_audio));

                                audio.src = tts_audio[chunkUrls[index]];
                                audio.load();

                                playPromise = audio.play();
                                playPromise
                                    .then(function () {
                                        // if playback succeeds, then add TTS control
                                        if (index === 0) {
                                            // add TTS control buttons
                                            AtKit.call("addTTSControl");

                                            // unfocus the clicked link
                                            $lib("div#sbar").on("click", "#at-lnk-TTSPausePlay", function () {
                                                this.blur();
                                            });
                                        }

                                        // close dialog
                                        AtKit.hideDialog();

                                        // fetch & cache next TTS audio
                                        if ((index + 1) < chunkUrls.length) {
                                            AtKit.call("fetchAndCacheTTSAudio", { chunkUrls: chunkUrls, index: index + 1 });
                                        }
                                    })
                                    .catch(function (error) {
                                        AtKit.message("<h2>" + AtKit.localisation("tts_error") + "</h2><p>" + error + "</p>");

                                        // remove TTS control buttons
                                        AtKit.call("removeTTSControl");
                                    });
                            };
                            // Load blob as Data URL
                            fileReader.readAsDataURL(blob);
                        })
                        .catch(function (error) {
                            AtKit.message("<h2>" + AtKit.localisation("tts_error") + "</h2><p>" + AtKit.localisation("tts_fetch_error") + error.message + "</p>");

                            // remove TTS control buttons
                            AtKit.call("removeTTSControl");
                        });
                }

                if ((index + 1) < chunkUrls.length) {
                    audio.onended = function () {
                        // play the next chunk of audio
                        AtKit.call("playChunksHtml5", { chunkUrls: chunkUrls, index: index + 1 });
                    };
                } else {
                    audio.onended = function () {
                        // remove TTS control buttons
                        AtKit.call("removeTTSControl");

                        // remove audio element
                        if (audio !== null) {
                            audio.parentNode.removeChild(audio);
                        }

                        // cleanup
                        window.sessionStorage.clear();
                    };
                }
            }
        });

        // A flag, if true, proceeed the TTS, false means TTS in progress
        AtKit.set("TTS_clickEnabled", true);

        AtKit.addButton(
            "tts",
            AtKit.localisation("tts_title"),
            AtKit.getPluginURL() + "images/sound.png",
            function (dialogs) {
                // remove control buttons
                AtKit.call("removeTTSControl");

                // cancel speaking utterance
                const synth = window.speechSynthesis;
                if (synth.speaking) {
                    synth.cancel();
                }

                // stop the HTML5 audio playback
                if (playPromise !== undefined && playPromise !== null) {
                    playPromise
                        .then(function () {
                            audio = document.getElementById("audio_tts");
                            if (audio !== undefined && audio !== null) {
                                audio.pause();

                                // cleanup
                                window.sessionStorage.clear();
                                playPromise = null;
                            }
                        });
                }

                // get selection text
                let text = AtKit.call("getSelectedTextInsipioTTS");

                // on clicking TTS toolbar button (and release the finger on touch screen), save selected text into variable
                if (text === "" && navigator.userAgent.match(/(iPhone|iPod|iPad)/i)) {
                    text = selectedDataIOS;
                }

                // If no text was selected, show a dialog with instructions.
                if (text === "") {
                    AtKit.show(dialogs.TTSSpeechSynthesisAPIRequiresSelection);
                    return;
                }

                // first, detect the string language
                AtKit.call("TTSSpeechSynthesisAPIGetStringLang", text).then(function (detected_lang) {
                    if (detected_lang === "ms") {
                        detected_lang = "id";// Malaysian voice is not supported, use Indonesian instead
                    }
                    if (AtKit.call("TTSSpeechSynthesisAPICheckSupport", detected_lang)) { // If the JS SSAPI is supported, use it
                        // Read the text using Speech Synthesis API
                        AtKit.call("TTSSpeechSynthesisAPIBeginReadText", { text: text, language: detected_lang });
                    } else { // Otherwise, fall back to the old way of doing things
                        AtKit.show(dialogs.options);
                        AtKit.set("TTS_Listeners_setup", false);

                        // set the detected language
                        let detected_language = detected_lang;
                        if (detected_language in cld_languages) {
                            detected_language = cld_languages[detected_language];
                        }
                        $lib("span#detected_language").html(detected_language + ".");

                        // load swfobject.js
                        if (window.isExtension) {
                            // tell background script to load local library
                            if (window.extensionBrowser === "chrome") {
                                chrome.runtime.sendMessage({ message: "load_local_library", filename: "/resources/js/swfobject.js" });
                            } else if (window.extensionBrowser === "firefox") {
                                browser.runtime.sendMessage({ message: "load_local_library", filename: "/resources/js/swfobject.js" });
                            } else if (window.extensionBrowser === "edge") {
                                browser.runtime.sendMessage({ message: "load_local_library", filename: "/resources/js/swfobject.js" });
                            }
                        } else {
                            AtKit.addScript(settings.baseURL + "resources/js/swfobject.js", null);
                        }

                        $lib("#sbStartInsipioTTSSelectionMale").on("click touchend", function () {
                            AtKit.call("sbStartInsipioTTSSelection", { voice: "male", text: text });
                        });
                        $lib("#sbStartInsipioTTSSelectionFemale").on("click touchend", function () {
                            AtKit.call("sbStartInsipioTTSSelection", { voice: "female", text: text });
                        });
                    }
                });
            },
            TTSDialogs,
            TTSFunctions
        );

        // speechSynthesis.getVoices() is loaded asynchronously. First call may return empty. We need to call it twice, or put it in the "onvoiceschanged" callback to make it work.
        window.speechSynthesis.onvoiceschanged = function () {
            window.speechSynthesis.getVoices();
        };

        // on IOS devices, periodically save selected text into variable every 100 ms (is this endless timer memory-safe?)
        if (navigator.userAgent.match(/(iPhone|iPod|iPad)/i)) {
            setInterval(function () {
                selectedDataIOS = AtKit.call("getSelectedTextInsipioTTS");
            }, 100);
        }
    };

    if (typeof window.AtKit === "undefined") {
        const AtKitLoaded = function () {
            var eventAction = null;

            this.subscribe = function (fn) {
                eventAction = fn;
            };

            this.fire = function (sender, eventArgs) {
                if (eventAction !== null) {
                    eventAction(sender, eventArgs);
                }
            };
        };

        window.AtKitLoaded = new AtKitLoaded();
        window.AtKitLoaded.subscribe(function () {
            AtKit.registerPlugin(pluginName, plugin);
        });
    } else {
        AtKit.registerPlugin(pluginName, plugin);
    }
}(window.AtKit));
