(function (AtKit) {
    var pluginName = "dictionary";
    var plugin = function () {
        var $lib = AtKit.lib();

        var settings = {
            serverURL: "https://spell.services.atbar.org/dictionary/"
        };

        // Internationalisation
        AtKit.addLocalisationMap("en", {
            dictionary_title: "Dictionary",
            dictionary_definition: "Dictionary definition for",
            dictionary_use: "To use the dictionary select a word on the page and click the dictionary button"
        });

        AtKit.addLocalisationMap("ar", {
            dictionary_title: "&#1575;&#1604;&#1605;&#1593;&#1580;&#1605;",
            dictionary_definition: "&#1578;&#1593;&#1585;&#1610;&#1601; &#1575;&#1604;&#1605;&#1593;&#1580;&#1605; &#1604;",
            dictionary_use: "&#1604;&#1575;&#1587;&#1578;&#1582;&#1583;&#1575;&#1605; &#1575;&#1604;&#1605;&#1593;&#1580;&#1605; &#1581;&#1583;&#1583; &#1603;&#1604;&#1605;&#1577; &#1593;&#1604;&#1609; &#1575;&#1604;&#1589;&#1601;&#1581;&#1577; &#1579;&#1605; &#1575;&#1590;&#1594;&#1591; &#1586;&#1585; &#1575;&#1604;&#1605;&#1593;&#1580;&#1605;"
        });


        // Add functions to AtKit.
        AtKit.addFn("getSelectedText", function (strip) {
            var text = "";
            if (document.selection && document.selection.type !== "Control") {
                text = document.selection.createRange().text;
            } else if (window.getSelection) {
                text = window.getSelection().toString();
            } else if (document.getSelection) {
                text = document.getSelection();
            }
            if (strip === true) {
                return String(text).replace(/([\s]+)/ig, "");
            }
            return String(text);
        });

        AtKit.addButton(
            "dictionary",
            AtKit.localisation("dictionary_title"),
            AtKit.getPluginURL() + "images/book_open.png",
            function () {
                var text = AtKit.call("getSelectedText");
                var stored = AtKit.get("DictionaryText");

                if (text === "" && stored !== undefined && stored.length > 0) {
                    text = stored;
                }

                const data = eval("\"" + text.split(" ").slice(0, 1) + "\";");// extract the first word
                if (typeof data === "string" && data.length > 0) {
                    $lib("#at-lnk-dictionary").children("img").attr("src", AtKit.getPluginURL() + "images/loading.gif");

                    if (window.isExtension) {
                        // using browser extension's cross-origin AJAX, instead of JSONP
                        $lib.ajax({
                            url: settings.serverURL + "xmlhttp/remote.php?titles=" + encodeURI(data.toLowerCase()) + "&v=2&l=" + AtKit.getLanguage() + "&callback=",
                            success: function (result_data) {
                                var definition = "";
                                var title = "";

                                result_data = result_data.substring(1);// trim left "("
                                result_data = result_data.substring(0, result_data.length - 2);// trim right ");"
                                result_data = JSON.parse(result_data);

                                const ro = result_data;
                                for (const result in ro.query.pages) {
                                    if (result > -1) {
                                        definition = eval("ro.query.pages[\"" + result + "\"].revisions[0][\"*\"];");
                                        title = eval("ro.query.pages[\"" + result + "\"].title;");
                                    } else {
                                        definition = "Unknown word";
                                        title = eval("ro.query.pages[\"-1\"].title;");
                                    }
                                }

                                AtKit.message("<h2>" + AtKit.localisation("dictionary_definition") + " \"" + title + "\"</h2><div style=\"height:300px; overflow-x:scroll\">" + definition + "</div>");
                                $lib("#at-lnk-dictionary").children("img").attr("src", AtKit.getPluginURL() + "images/book_open.png");
                            }
                        });
                    } else {
                        $lib.getJSON(settings.serverURL + "xmlhttp/remote.php?titles=" + encodeURI(data.toLowerCase()) + "&v=2&l=" + AtKit.getLanguage() + "&callback=?", function (result_data) {
                            var definition = "";
                            var title = "";
                            const ro = result_data;
                            for (const result in ro.query.pages) {
                                if (result > -1) {
                                    definition = eval("ro.query.pages[\"" + result + "\"].revisions[0][\"*\"];");
                                    title = eval("ro.query.pages[\"" + result + "\"].title;");
                                } else {
                                    definition = "Unknown word";
                                    title = eval("ro.query.pages[\"-1\"].title;");
                                }
                            }

                            AtKit.message("<h2>" + AtKit.localisation("dictionary_definition") + " \"" + title + "\"</h2><div style=\"height:300px; overflow-x:scroll\">" + definition + "</div>");
                            $lib("#at-lnk-dictionary").children("img").attr("src", AtKit.getPluginURL() + "images/book_open.png");
                        });
                    }
                } else {
                    AtKit.message("<h2>" + AtKit.localisation("dictionary_title") + "</h2><p>" + AtKit.localisation("dictionary_use") + "</p>");
                    $lib("#at-lnk-dictionary").children("img").attr("src", AtKit.getPluginURL() + "images/book_open.png");
                }
            },
            null, null
        );


        $lib("#at-btn-dictionary").mouseover(function () {
            AtKit.set("DictionaryText", AtKit.call("getSelectedText"));
        });
    };

    if (typeof window.AtKit === "undefined") {
        const AtKitLoaded = function () {
            var eventAction = null;

            this.subscribe = function (fn) {
                eventAction = fn;
            };

            this.fire = function (sender, eventArgs) {
                if (eventAction != null) {
                    eventAction(sender, eventArgs);
                }
            };
        };

        window.AtKitLoaded = new AtKitLoaded();
        window.AtKitLoaded.subscribe(function () { AtKit.registerPlugin(pluginName, plugin); });
    } else {
        AtKit.registerPlugin(pluginName, plugin);
    }
}(window.AtKit));
