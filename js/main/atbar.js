// start toolbar code
function __start() {
    (function (window, AtKit) {
        var toolbar_language = window.AtKitLanguage;
        var $lib = AtKit.lib();
        var plugins = ["resize", "fonts", "spellng", "dictionary", "insipio-tts", "wordprediction", "css", "overlay", "shortcutkeys", "tooltip"];
        var settings = {
            version: "2.2.2",
            resources: "https://core.atbar.org/resources/"
        };

        const onLoad = function () {
            let responsive_voice_img = "https://responsivevoice.org/wp-content/uploads/2014/08/95x15.png";
            if (window.isExtension) {
                if (window.extensionBrowser === "chrome") {
                    settings.resources = chrome.runtime.getURL("resources/");
                    responsive_voice_img = chrome.runtime.getURL("resources/img/responsivevoice-95x15.png");
                } else if (window.extensionBrowser === "firefox") {
                    settings.resources = browser.runtime.getURL("resources/");
                    responsive_voice_img = browser.runtime.getURL("resources/img/responsivevoice-95x15.png");
                } else if (window.extensionBrowser === "edge") {
                    settings.resources = browser.runtime.getURL("resources/");
                    responsive_voice_img = browser.runtime.getURL("resources/img/responsivevoice-95x15.png");
                }
            }

            // set the About string
            let about = "Version " + settings.version;
            about += "<p style=\"line-height:120%\">Created by Sebastian Skuse, Magnus White and the <a href='http://access.ecs.soton.ac.uk'>Accessibility Team</a>, Web and Internet Science, ECS<br> &copy; University of Southampton 2010-2018.";
            about += "<br><br>";
            about += "Fugue Icons &copy; <a href='http://www.pinvoke.com'>pinvoke</a> under Creative Commons licence.<br>";
            about += "Dictionary &copy; <a href='http://en.wiktionary.org'>Wiktionary</a> under Creative Commons licence.<br>";
            about += "<a href='http://defunkt.io/facebox/'>Facebox</a> jQuery plugin &copy; Chris Wanstrath under MIT licence<br>";
            about += "Portions of the spelling engine &copy; <a href=\"http://brandonaaron.net\">Brandon Aaron</a> under MIT licence.<br>";
            about += "Word prediction provided by <a href='http://www.aitype.com/'>AIType</a>.<br>";
            about += "Text-to-Speech provided by <a href='http://www.acapela-group.com/'>Acapela Group</a>, <a href=\"https://responsivevoice.org/\">ResponsiveVoice.JS</a> and <a href=\"https://translate.google.com/\">Google Translate</a>.";
            about += "</p>";
            about += "<div style=\"transition: none;\">";
            about += "<a target=\"_blank\" href=\"https://responsivevoice.org\">ResponsiveVoice-NonCommercial</a> licensed under<br>";
            about += "<a target=\"_blank\" href=\"https://creativecommons.org/licenses/by-nc-nd/4.0/\"><img title=\"ResponsiveVoice Text To Speech\" src=\"" + responsive_voice_img + "\" alt=\"95x15\" width=\"95\" height=\"15\" /></a>";
            about += "</div>";
            AtKit.setAbout(about);

            // Set our logo
            AtKit.setLogo(settings.resources + "img/atbar.png");
            AtKit.setName("ATBar");
            AtKit.setIsRightToLeft(false);

            // set the toolbar language
            if (toolbar_language === undefined) {
                toolbar_language = "en";// default to English
            } else if (toolbar_language === "ar") { // Arabic
                AtKit.setName("ATBar Arabic");
                AtKit.setIsRightToLeft(true);
                AtKit.addLocalisationMap("ar", {
                    exit: "&#1582;&#1585;&#1608;&#1580;",
                    reset: "&#1575;&#1587;&#1578;&#1593;&#1575;&#1583;&#1577; &#1575;&#1604;&#1608;&#1590;&#1593; &#1575;&#1604;&#1571;&#1589;&#1604;&#1610;"
                });
            }
            AtKit.setLanguage(toolbar_language);

            // Add all plugins to the toolbar
            $lib.each(plugins, function (i, v) {
                AtKit.addPlugin(v);
            });

            AtKit.addResetFn("reset-saved", function () {
                AtKit.clearStorage();
            });

            if (window.isExtension) {
                if (window.extensionBrowser === "chrome") {
                    AtKit.addUnloadFn("disable_atbar", function () {
                        // tell background script to disable ATbar
                        chrome.runtime.sendMessage({ message: "disable_atbar" });
                    });
                } else if (window.extensionBrowser === "firefox") {
                    AtKit.addUnloadFn("disable_atbar", function () {
                        // tell background script to disable ATbar
                        browser.runtime.sendMessage({ message: "disable_atbar" });
                    });
                } else if (window.extensionBrowser === "edge") {
                    AtKit.addUnloadFn("disable_atbar", function () {
                        // tell background script to disable ATbar
                        browser.runtime.sendMessage({ message: "disable_atbar" });
                    });
                }
            }

            // Run
            AtKit.render();

            // set facebox alignment
            if (AtKit.$.facebox !== undefined) {
                if (AtKit.settings.isRightToLeft) {
                    AtKit.$.facebox.settings.direction = "rtl";
                    AtKit.$.facebox.settings.closeDirection = "rtl";
                    AtKit.$.facebox.settings.textAlign = "right";
                    AtKit.$.facebox.settings.closeTextAlign = "right";
                } else {
                    AtKit.$.facebox.settings.direction = "ltr";
                    AtKit.$.facebox.settings.closeDirection = "ltr";
                    AtKit.$.facebox.settings.textAlign = "left";
                    AtKit.$.facebox.settings.closeTextAlign = "left";
                }
            }

            // Select the first button.
            // $lib(".at-btn:first a").focus();
        };

        if (window.isExtension) {
            if (window.extensionBrowser === "chrome") {
                // get toolbar language preference from localStorage
                let toolbar_lang;
                chrome.runtime.sendMessage({ message: "get_setting", key: "AtToolbarLanguage" }, function (response) {
                    toolbar_lang = response.value;
                    if (toolbar_lang !== null && toolbar_lang !== undefined) {
                        if (toolbar_lang !== "automatic") {
                            toolbar_language = toolbar_lang;
                        }
                    }

                    const current_toolbar_lang = AtKit.getLanguage();
                    if (current_toolbar_lang !== toolbar_language) {
                        // destroy toolbar, so that it'll be rebuilt with new language preference
                        AtKit.close(true);
                    }

                    // import all plugins
                    if (toolbar_language === "ar") {
                        plugins = ["resize", "fonts", "spellng", "insipio-tts", "wordprediction", "css", "overlay", "shortcutkeys", "tooltip"];
                    }
                    AtKit.importPlugins(plugins, onLoad);
                });
            } else if (window.extensionBrowser === "firefox") {
                // get toolbar language preference from localStorage
                let toolbar_lang;
                browser.runtime.sendMessage({ message: "get_setting", key: "AtToolbarLanguage" }).then(function (response) {
                    toolbar_lang = response.value;
                    if (toolbar_lang !== null && toolbar_lang !== undefined) {
                        if (toolbar_lang !== "automatic") {
                            toolbar_language = toolbar_lang;
                        }
                    }

                    const current_toolbar_lang = AtKit.getLanguage();
                    if (current_toolbar_lang !== toolbar_language) {
                        // destroy toolbar, so that it'll be rebuilt with new language preference
                        AtKit.close(true);
                    }

                    // import all plugins
                    if (toolbar_language === "ar") {
                        plugins = ["resize", "fonts", "spellng", "insipio-tts", "wordprediction", "css", "overlay", "shortcutkeys", "tooltip"];
                    }
                    AtKit.importPlugins(plugins, onLoad);
                });
            } else if (window.extensionBrowser === "edge") {
                // get toolbar language preference from localStorage
                let toolbar_lang;
                browser.runtime.sendMessage({ message: "get_setting", key: "AtToolbarLanguage" }, function (response) {
                    toolbar_lang = response.value;
                    if (toolbar_lang !== null && toolbar_lang !== undefined) {
                        if (toolbar_lang !== "automatic") {
                            toolbar_language = toolbar_lang;
                        }
                    }

                    const current_toolbar_lang = AtKit.getLanguage();
                    if (current_toolbar_lang !== toolbar_language) {
                        // destroy toolbar, so that it'll be rebuilt with new language preference
                        AtKit.close(true);
                    }

                    // import all plugins
                    if (toolbar_language === "ar") {
                        plugins = ["resize", "fonts", "spellng", "insipio-tts", "wordprediction", "css", "overlay", "shortcutkeys", "tooltip"];
                    }
                    AtKit.importPlugins(plugins, onLoad);
                });
            }
        } else {
            if (toolbar_language === "ar") {
                plugins = ["resize", "fonts", "spellng", "insipio-tts", "wordprediction", "css", "overlay", "shortcutkeys", "tooltip"];
            }
            AtKit.importPlugins(plugins, onLoad);
        }
    }(window, window.AtKit));
}

if (window.AtKit === undefined) {
    if (window.isExtension) {
        // tell background script to load atkit.js
        if (window.extensionBrowser === "chrome") {
            chrome.runtime.sendMessage({ message: "load_local_library", filename: "/js/main/atkit.js" });
        } else if (window.extensionBrowser === "firefox") {
            browser.runtime.sendMessage({ message: "load_local_library", filename: "/js/main/atkit.js" });
        } else if (window.extensionBrowser === "edge") {
            browser.runtime.sendMessage({ message: "load_local_library", filename: "/js/main/atkit.js" });
        }
    } else {
        // Load AtKit
        // d=document;jf=d.createElement('script');jf.src='https://core.atbar.org/atkit/latest/atkit.min.js';jf.type='text/javascript';jf.id='AtKitLib';d.getElementsByTagName('head')[0].appendChild(jf);
        const d = document;
        const jf = d.createElement("script");
        jf.src = "https://core.atbar.org/atkit/latest/atkit.min.js";
        jf.type = "text/javascript";
        jf.id = "AtKitLib";
        d.getElementsByTagName("head")[0].appendChild(jf);
    }

    const AtKitLoaded = function () {
        var eventAction = null;

        this.subscribe = function (fn) {
            eventAction = fn;
        };

        this.fire = function (sender, eventArgs) {
            if (eventAction !== null) {
                eventAction(sender, eventArgs);
            }
        };
    };

    window.AtKitLoaded = new AtKitLoaded();
    window.AtKitLoaded.subscribe(function () {
        __start();
    });
} else {
    __start();
}
