var activeTabId;
var activeWindowId;

var enabledTabs = [];// all URLs in which the ATbar is enabled

// get "AtEnabledSites" from Web Storage and convert into array
function getEnabledTabs() {
    const enabled_sites_str = window.localStorage.getItem("AtEnabledSites");
    if (enabled_sites_str !== null && enabled_sites_str !== undefined) {
        const temp_arr_enabled_sites = JSON.parse(enabled_sites_str);
        if (Array.isArray(temp_arr_enabled_sites) && temp_arr_enabled_sites.length > 0) {
            enabledTabs = temp_arr_enabled_sites;
        }
    }
    return enabledTabs;
}

// set icon badge based on Atbar state
function changeATbarIcon(hostname) {
    if (enabledTabs.indexOf(hostname) > -1) {
        chrome.browserAction.setBadgeText({ text: "ON" });
        chrome.browserAction.setBadgeBackgroundColor({ color: "#21AF5F" });
    } else {
        chrome.browserAction.setBadgeText({ text: "" });
    }
}

function startATBar(hostname) {
    // load atbar.js
    chrome.tabs.executeScript({ file: "js/main/atbar.js" });

    if (enabledTabs.indexOf(hostname) === -1) {
        enabledTabs.push(hostname);

        // store into Web Storage
        window.localStorage.setItem("AtEnabledSites", JSON.stringify(enabledTabs));
    }

    changeATbarIcon(hostname);
}

/**
 * Disable Atbar for the selected URL, then notify content script to destroy the toolbar
 * @param {string} url URL of the current active tab
 * @param {boolean} sendMessage Signal content script to call toolbar destructor
 */
function stopATBar(hostname, sendMessage) {
    const tab_url_position = enabledTabs.indexOf(hostname);
    if (tab_url_position > -1) {
        enabledTabs.splice(tab_url_position, 1);

        // store into Web Storage
        window.localStorage.setItem("AtEnabledSites", JSON.stringify(enabledTabs));
    }

    changeATbarIcon(hostname);

    // tell content script to destroy the toolbar
    if (sendMessage) {
        chrome.tabs.query({ active: true, currentWindow: true, status: "complete" }, function (tabs) {
            // if (tabs.length > 0 && activeTabId === tabs[0].id) {
            if (tabs.length > 0) {
                chrome.tabs.sendMessage(tabs[0].id, { message: "stop_atbar" });
            }
        });
    }
}

/**
 * Reload current active tab
 * @param {Boolean} bypassCache Whether using any local cache.
 */
function reload_tab(bypassCache) {
    if (bypassCache === undefined) {
        bypassCache = false;
    }

    chrome.tabs.reload({ bypassCache: bypassCache });
}

function loadLibraries(libraries) {
    libraries.forEach(function (library) {
        chrome.tabs.executeScript({ file: library });
    });
}

// called when the user clicks on the ATbar icon
chrome.browserAction.onClicked.addListener(function (tab) {
    activeTabId = tab.id;
    activeWindowId = tab.windowId;

    let parsedURL = { hostname: tab.url };
    parsedURL = new URL(tab.url);
    let hostname = parsedURL.hostname;
    if ((hostname === "" || hostname === "null") && tab.url.indexOf("file://") !== -1) {
        hostname = "file://";
    }

    getEnabledTabs();
    if (enabledTabs.length === 0 || enabledTabs.indexOf(hostname) === -1) {
        startATBar(hostname);
    } else {
        stopATBar(hostname, true);
    }
});

// Fired when a tab is updated
chrome.tabs.onUpdated.addListener(function (tabId, changeInfo, tab) {
    // chrome.tabs.executeScript({ code: "console.log('onUpdated event fired on tab: " + tabId + "');" });
    // chrome.tabs.executeScript({ code: "console.log('Active tab: " + activeTabId + "');" });

    if (activeTabId === undefined && activeWindowId === undefined) {
        chrome.tabs.query({ active: true, currentWindow: true, status: "complete" }, function (tabs) {
            if (tabs.length > 0) {
                activeTabId = tabs[0].id;
                activeWindowId = tabs[0].windowId;
            }
        });
    }

    if (changeInfo.status === "complete" && activeTabId === tab.id && activeWindowId === tab.windowId) {
        // // detect primary language on the current active tab
        // chrome.tabs.detectLanguage(function (language) {
        //     chrome.tabs.sendMessage(tab.id, {
        //         message: "detected_page_language",
        //         language: language
        //     });
        // });

        let parsedURL = { hostname: tab.url };
        parsedURL = new URL(tab.url);
        let hostname = parsedURL.hostname;
        if ((hostname === "" || hostname === "null") && tab.url.indexOf("file://") !== -1) {
            hostname = "file://";
        }

        getEnabledTabs();
        if (enabledTabs.indexOf(hostname) > -1) {
            startATBar(hostname);
        } else {
            changeATbarIcon(hostname);
        }
    }
});

// Fired when the active tab in a window changes
chrome.tabs.onActivated.addListener(function (activeInfo) {
    activeTabId = activeInfo.tabId;
    activeWindowId = activeInfo.windowId;

    chrome.tabs.query({ active: true, windowId: activeInfo.windowId, status: "complete" }, function (tabs) {
        if (tabs.length > 0 && activeTabId === tabs[0].id) {
            // // detect primary language on the current active tab
            // chrome.tabs.detectLanguage(function (language) {
            //     chrome.tabs.sendMessage(tabs[0].id, {
            //         message: "detected_page_language",
            //         language: language
            //     });
            // });

            let parsedURL = { hostname: tabs[0].url };
            parsedURL = new URL(tabs[0].url);
            let hostname = parsedURL.hostname;
            if ((hostname === "" || hostname === "null") && tabs[0].url.indexOf("file://") !== -1) {
                hostname = "file://";
            }

            getEnabledTabs();
            if (enabledTabs.indexOf(hostname) === -1) {
                stopATBar(hostname, true);
            } else {
                startATBar(hostname);
            }
        }
    });

    // chrome.tabs.executeScript({ code: "console.log('onActivated event fired on tab " + activeInfo.tabId + "');" });
    // chrome.tabs.executeScript({ code: "console.log('Active tab " + activeInfo.tabId + "');" });
});

chrome.contextMenus.onClicked.addListener(function (info, tab) {
    if (tab.id < 0) {
        // on local file scheme file:///*, tab contains incorrect value, so we query for active tab
        chrome.tabs.query({ active: true, currentWindow: true, status: "complete" }, function (tabs) {
            if (tabs.length > 0) {
                // start the ATbar
                let parsedURL = { hostname: tabs[0].url };
                parsedURL = new URL(tabs[0].url);
                let hostname = parsedURL.hostname;
                if ((hostname === "" || hostname === "null") && tabs[0].url.indexOf("file://") !== -1) {
                    hostname = "file://";
                }
                getEnabledTabs();
                if (enabledTabs.indexOf(hostname) === -1) {
                    startATBar(hostname);
                }

                // tell content script to proceed Text-to-Speech
                chrome.tabs.sendMessage(tabs[0].id, {
                    message: "tts",
                    selectionText: info.selectionText
                });
            }
        });
    } else {
        // start the ATbar
        let parsedURL = { hostname: tab.url };
        parsedURL = new URL(tab.url);
        let hostname = parsedURL.hostname;
        if ((hostname === "" || hostname === "null") && tab.url.indexOf("file://") !== -1) {
            hostname = "file://";
        }
        getEnabledTabs();
        if (enabledTabs.indexOf(hostname) === -1) {
            startATBar(hostname);
        }

        // tell content script to proceed Text-to-Speech
        chrome.tabs.sendMessage(tab.id, {
            message: "tts",
            selectionText: info.selectionText
        });
    }
});

// Set up context menu at install time.
chrome.runtime.onInstalled.addListener(function () {
    // create contextMenus
    chrome.contextMenus.create({
        title: "Text to Speech",
        contexts: ["selection"],
        id: "atbar_tts"
    });
});

// handle simple one-time requests
chrome.runtime.onMessage.addListener(
    function (request, sender, sendResponse) {
        if (request.message === "load_local_library") {
            // load the script
            chrome.tabs.executeScript({ file: request.filename });
        } else if (request.message === "load_multiple_local_libraries") {
            // load each script
            loadLibraries(request.filenames);
            sendResponse({ status: "ok" });
        } else if (request.message === "load_local_css") {
            // injects CSS into the page
            chrome.tabs.insertCSS({ file: request.filename });
        } else if (request.message === "reload_tab") {
            reload_tab(request.bypassCache);
        } else if (request.message === "disable_atbar") {
            // disable ATbar
            chrome.tabs.query({ active: true, currentWindow: true, status: "complete" }, function (tabs) {
                // if (tabs.length > 0 && activeTabId === tabs[0].id) {
                if (tabs.length > 0) {
                    let parsedURL = { hostname: tabs[0].url };
                    parsedURL = new URL(tabs[0].url);
                    let hostname = parsedURL.hostname;
                    if ((hostname === "" || hostname === "null") && tabs[0].url.indexOf("file://") !== -1) {
                        hostname = "file://";
                    }

                    getEnabledTabs();
                    stopATBar(hostname);
                }
            });
        } else if (request.message === "get_setting") {
            // get setting value from localStorage
            const setting_value = window.localStorage.getItem(request.key);
            sendResponse({ value: setting_value });
        }

        // return true;// send response asynchronously
    }
);
