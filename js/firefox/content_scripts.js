window.isExtension = true;
window.extensionBrowser = "firefox";
window.AtKitLanguage = "en";// default language
window.AtSelectionText = "";// selectionText by context menu

browser.runtime.onMessage.addListener(
    function (request, sender, sendResponse) {
        if (request.message === "stop_atbar") {
            // reload the page, or perhaps it's better to just close the toolbar ?
            // browser.runtime.sendMessage({ message: "reload_tab" });
            if (window.AtKit !== undefined) {
                window.AtKit.close(true);// destroy toolbar only
                sendResponse({ status: "ok" });
            }
        } else if (request.message === "detected_page_language") {
            // set AtKitLanguage based on the detected primary language in the current tab
            if (request.language !== undefined && request.language !== "und" && request.language !== "un") {
                window.AtKitLanguage = request.language;
            }
        } else if (request.message === "tts") {
            window.AtSelectionText = request.selectionText;
            if (window.AtKit !== undefined) {
                // call function which is bound to the TTS button
                if (Object.prototype.hasOwnProperty.call(window.AtKit.__env.buttons, "tts") && Object.prototype.hasOwnProperty.call(window.AtKit.__env.buttons.tts, "action") && Object.prototype.hasOwnProperty.call(window.AtKit.__env.buttons.tts, "dialogs")) {
                    window.AtKit.__env.buttons.tts.action(window.AtKit.__env.buttons.tts.dialogs);
                    sendResponse({ status: "ok" });
                }
            }
        }
    }
);
