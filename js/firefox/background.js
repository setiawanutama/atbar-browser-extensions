var activeTabId;
var activeWindowId;

var enabledTabs = [];// all URLs in which the ATbar is enabled

// get "AtEnabledSites" from Web Storage and convert into array
function getEnabledTabs() {
    const enabled_sites_str = window.localStorage.getItem("AtEnabledSites");
    if (enabled_sites_str !== null && enabled_sites_str !== undefined) {
        const temp_arr_enabled_sites = JSON.parse(enabled_sites_str);
        if (Array.isArray(temp_arr_enabled_sites) && temp_arr_enabled_sites.length > 0) {
            enabledTabs = temp_arr_enabled_sites;
        }
    }
    return enabledTabs;
}

// set icon badge based on Atbar state
function changeATbarIcon(hostname) {
    if (enabledTabs.indexOf(hostname) > -1) {
        browser.browserAction.setBadgeText({ text: "ON" });
        browser.browserAction.setBadgeBackgroundColor({ color: "#21AF5F" });
    } else {
        browser.browserAction.setBadgeText({ text: "" });
    }
}

function startATBar(hostname) {
    // load atbar.js
    browser.tabs.executeScript({ file: "/js/main/atbar.js" });

    if (enabledTabs.indexOf(hostname) === -1) {
        enabledTabs.push(hostname);

        // store into Web Storage
        window.localStorage.setItem("AtEnabledSites", JSON.stringify(enabledTabs));
    }

    changeATbarIcon(hostname);
}

/**
 * Disable Atbar for the selected URL, then notify content script to destroy the toolbar
 * @param {string} url URL of the current active tab
 * @param {boolean} sendMessage Signal content script to call toolbar destructor
 */
function stopATBar(hostname, sendMessage) {
    const tab_url_position = enabledTabs.indexOf(hostname);
    if (tab_url_position > -1) {
        enabledTabs.splice(tab_url_position, 1);

        // store into Web Storage
        window.localStorage.setItem("AtEnabledSites", JSON.stringify(enabledTabs));
    }

    changeATbarIcon(hostname);

    // tell content script to destroy the toolbar
    if (sendMessage) {
        browser.tabs.query({ active: true, currentWindow: true, status: "complete" }).then(function (tabs) {
            // if (tabs.length > 0 && activeTabId === tabs[0].id) {
            if (tabs.length > 0) {
                browser.tabs.sendMessage(tabs[0].id, { message: "stop_atbar" });
            }
        });
    }
}

/**
 * Reload current active tab
 * @param {Boolean} bypassCache Whether using any local cache.
 */
function reload_tab(bypassCache) {
    if (bypassCache === undefined) {
        bypassCache = false;
    }

    browser.tabs.reload({ bypassCache: bypassCache });
}

function loadLibraries(libraries) {
    libraries.forEach(function (library) {
        browser.tabs.executeScript({ file: library });
    });
}

// called when the user clicks on the ATbar icon
browser.browserAction.onClicked.addListener(function (tab) {
    activeTabId = tab.id;
    activeWindowId = tab.windowId;

    let activeURL;
    if (tab.url === undefined) {
        activeURL = document.location.href;
    } else {
        activeURL = tab.url;
    }

    let parsedURL = { hostname: activeURL };
    parsedURL = new URL(activeURL);
    let hostname = parsedURL.hostname;
    if ((hostname === "" || hostname === "null") && activeURL.indexOf("file://") !== -1) {
        hostname = "file://";
    }

    getEnabledTabs();
    if (enabledTabs.length === 0 || enabledTabs.indexOf(hostname) === -1) {
        startATBar(hostname);
    } else {
        stopATBar(hostname, true);
    }
});

// Fired when a tab is updated
browser.tabs.onUpdated.addListener(function (tabId, changeInfo, tab) {
    if (activeTabId === undefined && activeWindowId === undefined) {
        browser.tabs.query({ active: true, currentWindow: true }).then(function (tabs) {
            if (tabs.length > 0) {
                activeTabId = tabs[0].id;
                activeWindowId = tabs[0].windowId;
            }
        });
    }

    if (changeInfo.status === "complete" && activeTabId === tab.id && activeWindowId === tab.windowId) {
        // browser.tabs.executeScript({ code: "console.log('onUpdated event fired on tab: " + tabId + "');" });
        // browser.tabs.executeScript({ code: "console.log('Active tab: " + activeTabId + "');" });

        let activeURL;
        if (tab.url === undefined) {
            activeURL = document.location.href;
        } else {
            activeURL = tab.url;
        }

        let parsedURL = { hostname: activeURL };
        parsedURL = new URL(activeURL);
        let hostname = parsedURL.hostname;
        if ((hostname === "" || hostname === "null") && activeURL.indexOf("file://") !== -1) {
            hostname = "file://";
        }

        let is_local_pdf_viewer = false;
        // algorithm is taken from https://benohead.com/getting-a-file-extension-with-javascript/
        const filename = activeURL.toLowerCase().split("\\").pop().split("/")
            .pop();// extract filename in lowercase
        const file_ext = filename.substr((Math.max(0, filename.lastIndexOf(".")) || Infinity) + 1);// extract the file extension
        if (file_ext === "pdf") {
            is_local_pdf_viewer = true;
        }

        if (!is_local_pdf_viewer) {
            // detect primary language on the current active tab
            browser.tabs.detectLanguage().then(function (language) {
                browser.tabs.sendMessage(tab.id, {
                    message: "detected_page_language",
                    language: language
                });
            });

            getEnabledTabs();
            if (enabledTabs.indexOf(hostname) > -1) {
                startATBar(hostname);
            } else {
                stopATBar(hostname, true);
            }
        } else {
            browser.browserAction.setBadgeText({ text: "" });
        }
    }
});

// Fired when the active tab in a window changes
browser.tabs.onActivated.addListener(function (activeInfo) {
    activeTabId = activeInfo.tabId;
    activeWindowId = activeInfo.windowId;

    browser.tabs.query({ active: true, windowId: activeInfo.windowId, status: "complete" }).then(function (tabs) {
        if (tabs.length > 0 && activeTabId === tabs[0].id) {
            let activeURL;
            if (tabs[0].url === undefined) {
                activeURL = document.location.href;
            } else {
                activeURL = tabs[0].url;
            }

            let parsedURL = { hostname: activeURL };
            parsedURL = new URL(activeURL);
            let hostname = parsedURL.hostname;
            if ((hostname === "" || hostname === "null") && activeURL.indexOf("file://") !== -1) {
                hostname = "file://";
            }

            let is_local_pdf_viewer = false;
            // algorithm is taken from https://benohead.com/getting-a-file-extension-with-javascript/
            const filename = activeURL.toLowerCase().split("\\").pop().split("/")
                .pop();// extract filename in lowercase
            const file_ext = filename.substr((Math.max(0, filename.lastIndexOf(".")) || Infinity) + 1);// extract the file extension
            if (file_ext === "pdf") {
                is_local_pdf_viewer = true;
            }

            if (!is_local_pdf_viewer) {
                // detect primary language on the current active tab
                browser.tabs.detectLanguage().then(function (language) {
                    browser.tabs.sendMessage(tabs[0].id, {
                        message: "detected_page_language",
                        language: language
                    });
                });

                getEnabledTabs();
                if (enabledTabs.indexOf(hostname) === -1) {
                    stopATBar(hostname, true);
                } else {
                    startATBar(hostname);
                }
            } else {
                browser.browserAction.setBadgeText({ text: "" });
            }
        }
    });

    // browser.tabs.executeScript({ code: "console.log('onActivated event fired on tab " + activeInfo.tabId + "');" });
    // browser.tabs.executeScript({ code: "console.log('Active tab " + activeInfo.tabId + "');" });
});

browser.contextMenus.onClicked.addListener(function (info, tab) {
    if (tab.id < 0) {
        // on local file scheme file:///*, tab may contain incorrect value, so we query for active tab
        browser.tabs.query({ active: true, currentWindow: true, status: "complete" }).then(function (tabs) {
            if (tabs.length > 0) {
                // tell content script to proceed Text-to-Speech
                browser.tabs.sendMessage(tabs[0].id, { message: "tts", selectionText: info.selectionText }).then(function () {
                    // start the ATbar
                    let activeURL;
                    if (tabs[0].url === undefined) {
                        activeURL = document.location.href;
                    } else {
                        activeURL = tabs[0].url;
                    }

                    let parsedURL = { hostname: activeURL };
                    parsedURL = new URL(activeURL);
                    let hostname = parsedURL.hostname;
                    if ((hostname === "" || hostname === "null") && activeURL.indexOf("file://") !== -1) {
                        hostname = "file://";
                    }

                    let is_local_pdf_viewer = false;
                    // algorithm is taken from https://benohead.com/getting-a-file-extension-with-javascript/
                    const filename = activeURL.toLowerCase().split("\\").pop().split("/")
                        .pop();// extract filename in lowercase
                    const file_ext = filename.substr((Math.max(0, filename.lastIndexOf(".")) || Infinity) + 1);// extract the file extension
                    if (file_ext === "pdf") {
                        is_local_pdf_viewer = true;
                    }

                    if (!is_local_pdf_viewer) {
                        getEnabledTabs();
                        if (enabledTabs.indexOf(hostname) === -1) {
                            startATBar(hostname);
                        }
                    }
                });
            }
        });
    } else {
        // tell content script to proceed Text-to-Speech
        browser.tabs.sendMessage(tab.id, { message: "tts", selectionText: info.selectionText }).then(function () {
            // start the ATbar
            let activeURL;
            if (tab.url === undefined) {
                activeURL = document.location.href;
            } else {
                activeURL = tab.url;
            }

            let parsedURL = { hostname: activeURL };
            parsedURL = new URL(activeURL);
            let hostname = parsedURL.hostname;
            if ((hostname === "" || hostname === "null") && activeURL.indexOf("file://") !== -1) {
                hostname = "file://";
            }

            let is_local_pdf_viewer = false;
            // algorithm is taken from https://benohead.com/getting-a-file-extension-with-javascript/
            const filename = activeURL.toLowerCase().split("\\").pop().split("/")
                .pop();// extract filename in lowercase
            const file_ext = filename.substr((Math.max(0, filename.lastIndexOf(".")) || Infinity) + 1);// extract the file extension
            if (file_ext === "pdf") {
                is_local_pdf_viewer = true;
            }

            if (!is_local_pdf_viewer) {
                getEnabledTabs();
                if (enabledTabs.indexOf(hostname) === -1) {
                    startATBar(hostname);
                }
            }
        });
    }
});

// Set up context menu at install time.
browser.runtime.onInstalled.addListener(function () {
    // create contextMenus
    browser.contextMenus.create({
        title: "Text to Speech",
        contexts: ["selection"],
        id: "atbar_tts"
    });
});

// handle simple one-time requests
browser.runtime.onMessage.addListener(
    function (request, sender, sendResponse) {
        if (request.message === "load_local_library") {
            // load the script
            browser.tabs.executeScript({ file: request.filename });
        } else if (request.message === "load_multiple_local_libraries") {
            // load each script
            loadLibraries(request.filenames);
            sendResponse({ status: "ok" });
        } else if (request.message === "load_local_css") {
            // injects CSS into the page
            browser.tabs.insertCSS({ file: request.filename });
        } else if (request.message === "reload_tab") {
            reload_tab(request.bypassCache);
        } else if (request.message === "disable_atbar") {
            // disable ATbar
            browser.tabs.query({ active: true, currentWindow: true, status: "complete" }).then(function (tabs) {
                // if (tabs.length > 0 && activeTabId === tabs[0].id) {
                if (tabs.length > 0) {
                    let activeURL;
                    if (tabs[0].url === undefined) {
                        activeURL = document.location.href;
                    } else {
                        activeURL = tabs[0].url;
                    }

                    let parsedURL = { hostname: activeURL };
                    parsedURL = new URL(activeURL);
                    let hostname = parsedURL.hostname;
                    if ((hostname === "" || hostname === "null") && activeURL.indexOf("file://") !== -1) {
                        hostname = "file://";
                    }

                    getEnabledTabs();
                    stopATBar(hostname);
                }
            });
        } else if (request.message === "get_setting") {
            // get setting value from localStorage
            const setting_value = window.localStorage.getItem(request.key);
            sendResponse({ value: setting_value });
        }

        // return true;// send response asynchronously
    }
);
