// implemented using JQuery
$(function () {
    var activeTabId;
    var activeURL = "";
    var activeHostname = "";
    var fav_icon = "";

    var enabledTabs = [];// all URLs in which the ATbar is enabled

    // get "AtEnabledSites" from Web Storage and convert into array
    function getEnabledTabs() {
        const enabled_sites_str = window.localStorage.getItem("AtEnabledSites");
        if (enabled_sites_str !== null && enabled_sites_str !== undefined) {
            const temp_arr_enabled_sites = JSON.parse(enabled_sites_str);
            if (Array.isArray(temp_arr_enabled_sites) && temp_arr_enabled_sites.length > 0) {
                enabledTabs = temp_arr_enabled_sites;
            }
        }
        return enabledTabs;
    }

    // set icon badge based on Atbar state
    function changeATbarIcon(hostname) {
        if (enabledTabs.indexOf(hostname) > -1) {
            browser.browserAction.setBadgeText({ text: "ON" });
            browser.browserAction.setBadgeBackgroundColor({ color: "#21AF5F" });
        } else {
            browser.browserAction.setBadgeText({ text: "" });
        }
    }

    function startATBar(hostname) {
        // load atbar.js
        browser.tabs.executeScript({ file: "/js/main/atbar.js" });

        if (enabledTabs.indexOf(hostname) === -1) {
            enabledTabs.push(hostname);

            // store into Web Storage
            window.localStorage.setItem("AtEnabledSites", JSON.stringify(enabledTabs));
        }

        changeATbarIcon(hostname);
    }

    /**
     * Disable Atbar for the selected URL, then notify content script to destroy the toolbar
     * @param {string} url URL of the current active tab
     * @param {boolean} sendMessage Signal content script to call toolbar destructor
     */
    function stopATBar(hostname, sendMessage) {
        const tab_url_position = enabledTabs.indexOf(hostname);
        if (tab_url_position > -1) {
            enabledTabs.splice(tab_url_position, 1);

            // store into Web Storage
            window.localStorage.setItem("AtEnabledSites", JSON.stringify(enabledTabs));
        }

        changeATbarIcon(hostname);

        // tell content script to destroy the toolbar
        if (sendMessage) {
            browser.tabs.query({ active: true, currentWindow: true, status: "complete" }, function (tabs) {
                // if (tabs.length > 0 && activeTabId === tabs[0].id) {
                if (tabs.length > 0) {
                    browser.tabs.sendMessage(tabs[0].id, { message: "stop_atbar" });
                }
            });
        }
    }

    // get active tab information
    browser.tabs.query({ active: true, currentWindow: true }, function (tabs) {
        if (tabs.length > 0) {
            activeTabId = tabs[0].id;

            if (tabs[0].url === undefined) {
                activeURL = document.location.href;
            } else {
                activeURL = tabs[0].url;
            }

            let parsedURL = { hostname: activeURL };
            parsedURL = new URL(activeURL);
            activeHostname = parsedURL.hostname;
            if ((activeHostname === "" || activeHostname === "null") && activeURL.indexOf("file://") !== -1) {
                activeHostname = "file://";
            }

            let is_local_pdf_viewer = false;
            // algorithm is taken from https://benohead.com/getting-a-file-extension-with-javascript/
            const filename = activeURL.toLowerCase().split("\\").pop().split("/")
                .pop();// extract filename in lowercase
            const file_ext = filename.substr((Math.max(0, filename.lastIndexOf(".")) || Infinity) + 1);// extract the file extension
            if (file_ext === "pdf") {
                is_local_pdf_viewer = true;
            }

            // can't inject content script to internal PDF viewer plugin, so only detect language if content script is active
            if (!is_local_pdf_viewer) {
                // detect primary language on the current active tab
                browser.tabs.detectLanguage(function (language) {
                    browser.tabs.sendMessage(tabs[0].id, {
                        message: "detected_page_language",
                        language: language
                    });
                });
            }

            fav_icon = tabs[0].favIconUrl;

            if (activeURL.indexOf("ms-browser-extension://") !== -1 || is_local_pdf_viewer) { // not supported in extension page
                $("#atbar-toggle").removeClass("_1TCaF").addClass("b519s");
                $("#atbar-toggle div._1SBFw").addClass("_2mg9_");
                // hide fav-icon img
                $("#atbar-toggle span.CKnv8 img").hide();
                $("#atbar-toggle span#atbar-toggle-caption").text("ATbar is not supported");
                $("#atbar-toggle span#hostname").html("<b>privileged page</b>");
            } else {
                $("#atbar-toggle").removeClass("b519s").addClass("_1TCaF");
                $("#atbar-toggle div._1SBFw").removeClass("_2mg9_");
                $("#atbar-toggle span#atbar-toggle-caption").text("Enable ATbar");
                if (activeHostname !== "") {
                    $("#atbar-toggle span#hostname").html("<b>" + activeHostname + "</b>");
                }

                getEnabledTabs();
                if (enabledTabs.length === 0 || enabledTabs.indexOf(activeHostname) === -1) {
                    // ATbar is OFF
                    $("#atbar-toggle input[type=checkbox]").prop("checked", false);
                } else {
                    // ATbar is ON
                    $("#atbar-toggle input[type=checkbox]").prop("checked", true);
                }
            }

            if (fav_icon !== undefined && fav_icon !== "") {
                // show fav-icon
                $("#atbar-toggle span.CKnv8 img").attr("src", fav_icon);
                $("#atbar-toggle span.CKnv8 img").show();
            } else {
                // hide fav-icon img
                $("#atbar-toggle span.CKnv8 img").hide();
            }
        }
    });

    // Toggle ATbar On-Off
    $("#atbar-toggle input[type=checkbox]").on("change", function () {
        getEnabledTabs();
        if ($(this).is(":checked")) {
            // enable ATbar
            startATBar(activeHostname);
        } else {
            // disable ATbar
            stopATBar(activeHostname, true);
        }
    });

    // get split-long-text setting from localStorage
    let split_long_text;
    split_long_text = window.localStorage.getItem("AtTTSSplitLongText");
    if (split_long_text !== null && split_long_text !== undefined) {
        split_long_text = Boolean(split_long_text === "true");
    } else {
        split_long_text = true;// default to true
    }
    if (split_long_text) {
        $("input#tts_split_long_text").prop("checked", true);
    } else {
        $("input#tts_split_long_text").prop("checked", false);
    }
    // event handler for TTS - Split long text
    $("input#tts_split_long_text").on("change", function () {
        if ($(this).is(":checked")) {
            split_long_text = true;
        } else {
            split_long_text = false;
        }

        // save setting to localStorage
        window.localStorage.setItem("AtTTSSplitLongText", split_long_text.toString());
    });

    // get TTS service preference from localStorage
    let tts_service = window.localStorage.getItem("AtTTSService");
    if (tts_service !== null) {
        $("select#tts-service option[value=\"" + tts_service + "\"]").prop("selected", true);
    }
    // event handler for TTS service preference
    $("select#tts-service").on("change", function () {
        tts_service = $(this).find("option:selected").attr("value");

        // save setting to localStorage
        window.localStorage.setItem("AtTTSService", tts_service);
    });

    // get voice language preference
    let voice_language = window.localStorage.getItem("AtTTSVoiceLanguage");
    if (voice_language !== null) {
        $("select#voice-language option[value=\"" + voice_language + "\"]").prop("selected", true);
    }
    // event handler for voice language preference
    $("select#voice-language").on("change", function () {
        voice_language = $(this).find("option:selected").attr("value");

        // save setting to localStorage
        window.localStorage.setItem("AtTTSVoiceLanguage", voice_language);
    });

    // get toolbar language preference from localStorage
    let toolbar_lang = window.localStorage.getItem("AtToolbarLanguage");
    if (toolbar_lang !== null) {
        $("select#toolbar-lang option[value=\"" + toolbar_lang + "\"]").prop("selected", true);
    }
    // event handler for toolbar language preference
    $("select#toolbar-lang").on("change", function () {
        toolbar_lang = $(this).find("option:selected").attr("value");

        // save setting to localStorage
        window.localStorage.setItem("AtToolbarLanguage", toolbar_lang);

        let is_local_pdf_viewer = false;
        // algorithm is taken from https://benohead.com/getting-a-file-extension-with-javascript/
        const filename = activeURL.toLowerCase().split("\\").pop().split("/")
            .pop();// extract filename in lowercase
        const file_ext = filename.substr((Math.max(0, filename.lastIndexOf(".")) || Infinity) + 1);// extract the file extension
        if (file_ext === "pdf") {
            is_local_pdf_viewer = true;
        }

        if (!is_local_pdf_viewer) {
            getEnabledTabs();
            if (enabledTabs.indexOf(activeHostname) > -1 && activeURL.indexOf("ms-browser-extension://") === -1) {
                // ATbar is ON, restart to take effect with new language preference
                // destroy toolbar, then rebuild
                browser.tabs.sendMessage(activeTabId, { message: "stop_atbar" }, function () {
                    startATBar(activeHostname);
                });
            }
        }
    });
});
