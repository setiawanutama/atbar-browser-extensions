var gulp = require("gulp");
var rename = require("gulp-rename");
var zip = require("gulp-zip");

/**
 * Export to dist/chrome folder containing Chrome browser extension files and manifest.json, and zip it to chrome.zip in dist folder
 */
gulp.task("chrome_manifest", function () {
    return gulp.src(["manifest-chrome.json"], { base: "." })
        .pipe(rename("manifest.json"))
        .pipe(gulp.dest("dist/chrome"));
});
gulp.task("chrome", function () {
    return gulp.src(["icons/**/*", "resources/**/*", "js/chrome/**/*", "js/lib/**/*", "js/main/**/*", "popup-chrome.html"], { base: "." })
        .pipe(gulp.dest("dist/chrome"));
});
gulp.task("chrome_zip", gulp.series("chrome_manifest", "chrome", () => gulp.src(["dist/chrome/**/*"])
    .pipe(zip("chrome.zip"))
    .pipe(gulp.dest("dist"))));

/**
 * Export to dist/firefox folder containing Firefox browser extension files and manifest.json, and zip it to firefox.zip in dist folder
 */
gulp.task("firefox_manifest", function () {
    return gulp.src(["manifest-firefox.json"], { base: "." })
        .pipe(rename("manifest.json"))
        .pipe(gulp.dest("dist/firefox"));
});
gulp.task("firefox", function () {
    return gulp.src(["icons/**/*", "resources/**/*", "js/firefox/**/*", "js/lib/**/*", "js/main/**/*", "popup-firefox.html"], { base: "." })
        .pipe(gulp.dest("dist/firefox"));
});
gulp.task("firefox_zip", gulp.series("firefox_manifest", "firefox", () => gulp.src(["dist/firefox/**/*"])
    .pipe(zip("firefox.zip"))
    .pipe(gulp.dest("dist"))));

/**
 * Export to dist/opera folder containing Opera browser extension files and manifest.json, and zip it to opera.zip in dist folder
 */
gulp.task("opera_manifest", function () {
    return gulp.src(["manifest-opera.json"], { base: "." })
        .pipe(rename("manifest.json"))
        .pipe(gulp.dest("dist/opera"));
});
gulp.task("opera", function () {
    return gulp.src(["icons/**/*", "resources/**/*", "js/opera/**/*", "js/lib/**/*", "js/main/**/*", "popup-opera.html"], { base: "." })
        .pipe(gulp.dest("dist/opera"));
});
gulp.task("opera_zip", gulp.series("opera_manifest", "opera", () => gulp.src(["dist/opera/**/*"])
    .pipe(zip("opera.zip"))
    .pipe(gulp.dest("dist"))));

/**
 * Export to dist/edge folder containing Edge browser extension files and manifest.json, and zip it to edge.zip in dist folder
 */
gulp.task("edge_manifest", function () {
    return gulp.src(["manifest-edge.json"], { base: "." })
        .pipe(rename("manifest.json"))
        .pipe(gulp.dest("dist/edge"));
});
gulp.task("edge", function () {
    return gulp.src(["icons/**/*", "resources/**/*", "js/edge/**/*", "js/lib/**/*", "js/main/**/*", "popup-edge.html"], { base: "." })
        .pipe(gulp.dest("dist/edge"));
});
gulp.task("edge_zip", gulp.series("edge_manifest", "edge", () => gulp.src(["dist/edge/**/*"])
    .pipe(zip("edge.zip"))
    .pipe(gulp.dest("dist"))));

// default task
gulp.task("default", gulp.parallel("chrome_zip", "firefox_zip", "opera_zip", "edge_zip"));
